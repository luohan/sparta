/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "math.h"
#include "pointers.h"
#include "string.h"
#include "stdlib.h"
#include "react_tce.h"
#include "particle.h"
#include "collide.h"
#include "random_park.h"
#include "error.h"


// DEBUG
#include "update.h"
#include <cstdio>
#include <math.h>



using namespace SPARTA_NS;

enum{DISSOCIATION,EXCHANGE,IONIZATION,RECOMBINATION};   // other files

/* ---------------------------------------------------------------------- */

ReactTCE::ReactTCE(SPARTA *sparta, int narg, char **arg) :
  ReactBird(sparta, narg, arg) {}

/* ---------------------------------------------------------------------- */

void ReactTCE::init()
{
  if (!collide || strcmp(collide->style,"vss") != 0)
    error->all(FLERR,"React tce can only be used with collide vss");

  ReactBird::init();
}

/* ---------------------------------------------------------------------- */

int ReactTCE::attempt(Particle::OnePart *ip, Particle::OnePart *jp,
                      double pre_etrans, double pre_erot, double pre_evib,
                      double &post_etotal, int &kspecies)
{
  double pre_etotal,ecc,e_excess;
  OneReaction *r;

  Particle::Species *species = particle->species;
  int isp = ip->ispecies;
  int jsp = jp->ispecies;

  double pre_ave_rotdof = (species[isp].rotdof + species[jsp].rotdof)/2.0;

  double pre_ave_vibdof = 0.0;

  int n = reactions[isp][jsp].n;
  if (n == 0) return 0;
  int *list = reactions[isp][jsp].list;

  // get cell temperature and calculate pre_ave_vibdof

  double total_vibmode = species[isp].nvibmode + species[jsp].nvibmode;
  double *cell_temp;
  if (vdofflag && total_vibmode > 0) {
    cell_temp = new double[collide->ncell_temp];
    collide->get_cell_temp(jp->icell, cell_temp);
    for (int i = 0; i < 2; i++) {
      int ii = isp;
      if (i == 1) ii = jsp;
      Particle::Species *sp = &species[ii];
      if (sp->nvibmode == 1) {
        double Tv = cell_temp[2];
        if (collide->ncell_temp > 2) Tv = cell_temp[2+ii];
        double x = sp->vibtemp[0] / Tv;
        pre_ave_vibdof += 2.0 * x / (exp(x) - 1.0);
      }
    }
    pre_ave_vibdof *= 0.5;
  }



  // probablity to compare to reaction probability

  double react_prob = 0.0;
  double random_prob = random->uniform();

  // loop over possible reactions for these 2 species

  for (int i = 0; i < n; i++) {
    r = &rlist[list[i]];

    // ignore energetically impossible reactions

    pre_etotal = pre_etrans + pre_erot + pre_evib;

    ecc = pre_etrans;
    if (pre_ave_rotdof > 0.1) ecc += pre_erot*r->coeff[0]/pre_ave_rotdof;
    if (vdofflag && total_vibmode > 0) ecc += pre_evib;

    e_excess = ecc - r->coeff[1];
    if (e_excess <= 0.0) continue;

    double vib_correct = 1.0;
    double den = r->coeff[5] + r->coeff[3] + 1.0;  // zeta + eta + 3/2
    if (vdofflag && total_vibmode > 0) {
      double nom = r->coeff[5] + 1.0;                // zeta + 5/2 + omega
      vib_correct = tgamma(den) / tgamma(nom) * tgamma(nom + pre_ave_vibdof) / tgamma(den + pre_ave_vibdof);
    }

    if (den + pre_ave_vibdof < 1.0) {
      char s[120];
      sprintf(s, "Reaction %s doesn't satisfy eta + zeta + 0.5 > 0, check Bird Page 128", r->id);
      error->all(FLERR, s);
    }

    // compute probability of reaction

    switch (r->type) {
    case DISSOCIATION:
    case IONIZATION:
    case EXCHANGE:
      {
        react_prob += r->coeff[2] *
          pow(ecc-r->coeff[1],r->coeff[3]) *
          pow(1.0-r->coeff[1]/ecc,r->coeff[5] + pre_ave_vibdof) * vib_correct;
        break;
      }

    case RECOMBINATION:
      {
        // skip if no 3rd particle chosen by Collide::collisions()
        //   this includes effect of boost factor to skip recomb reactions
        // check if this recomb reaction is the same one
        //   that the 3rd particle species maps to, else skip it
        // this effectively skips all recombinations reactions
        //   if selected a 3rd particle species that matches none of them
        // scale probability by boost factor to restore correct stats

        if (recomb_species < 0) continue;
        int *sp2recomb = reactions[isp][jsp].sp2recomb;
        if (sp2recomb[recomb_species] != list[i]) continue;

        react_prob += recomb_boost * recomb_density * r->coeff[2] *
          pow(ecc,r->coeff[3]) *
          pow(1.0-r->coeff[1]/ecc,r->coeff[5] + pre_ave_vibdof) * vib_correct;
        break;
      }

    default:
      error->one(FLERR,"Unknown outcome in reaction");
      break;
    }

    // test against random number to see if this reaction occurs
    // if it does, reset species of I,J and optional K to product species
    // J particle can be destroyed in recombination reaction, set species = -1
    // K particle can be created in a dissociation or ionization reaction,
    //   set its kspecies, parent will create it
    // important NOTE:
    //   does not matter what order I,J reactants are in compared
    //     to order the reactants are listed in the reaction file
    //   for two reasons:
    //   a) list of N possible reactions above includes all reactions
    //      that I,J species are in, regardless of order
    //   b) properties of pre-reaction state, stored in precoln,
    //      as computed by setup_collision(),
    //      and used by perform_collision() after reaction has taken place,
    //      only store combined properties of I,J,
    //      nothing that is I-specific or J-specific

    if (react_prob > random_prob) {
      tally_reactions[list[i]]++;

      if (!frozenflag) {
        ip->ispecies = r->products[0];

        // Previous statment did not destroy the 2nd species (B) if
        //   recombination was specified as A+B->AB+M (which has nproductus=2)
        //   but only for the A+B->AB specication form (which has nproductus=1)

        switch (r->type) {
        case DISSOCIATION:
        case IONIZATION:
        case EXCHANGE:
          {
            jp->ispecies = r->products[1];
            break;
          }
        case RECOMBINATION:
          {
            // always destroy 2nd reactant species

            jp->ispecies = -1;
            break;
          }
        }

        if (r->nproduct > 2) kspecies = r->products[2];
        else kspecies = -1;

        post_etotal = pre_etotal + r->coeff[4];
      } else {
        post_etotal = pre_etotal;
        kspecies = -1;
      }

      return 1;
    }
  }

  if (vdofflag && total_vibmode > 0) {
    delete [] cell_temp;
  }
  return 0;
}

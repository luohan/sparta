/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "math.h"
#include "math_extra.h"
#include "string.h"
#include "stdlib.h"
#include "collide_vss.h"
#include "grid.h"
#include "update.h"
#include "particle.h"
#include "mixture.h"
#include "collide.h"
#include "react.h"
#include "comm.h"
#include "fix_vibmode.h"
#include "random_park.h"
#include "math_const.h"
#include "memory.h"
#include "error.h"
#include <algorithm>

using namespace SPARTA_NS;
using namespace MathConst;

enum{NONE,DISCRETE,SMOOTH};            // several files
enum{CONSTANT,VARIABLE};
enum{ECRELAX,TRELAX};

#define MAXLINE 1024
#define pairparams2D(i,j) ((i <= j)?pairparams[i*nparams + j]:pairparams[j*nparams + i])
#define spairparams2D(i,j) ((i <= j)?spairparams[i*nparams + j]:spairparams[j*nparams + i])

/* ---------------------------------------------------------------------- */

CollideVSS::CollideVSS(SPARTA *sparta, int narg, char **arg) :
  Collide(sparta, narg, arg)
{
  if (narg < 3) error->all(FLERR,"Illegal collide command");

  // optional args
  relaxflag = CONSTANT;
  isothermal_flag = 0;
  relaxtemp = ECRELAX;

  int iarg = 3;
  while (iarg < narg) {
    if (strcmp(arg[iarg],"relax") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal collide command");
      if (strcmp(arg[iarg+1],"constant") == 0) relaxflag = CONSTANT;
      else if (strcmp(arg[iarg+1],"variable") == 0) relaxflag = VARIABLE;
      else error->all(FLERR,"Illegal collide command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"isothermal") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal collide command");
      if (strcmp(arg[iarg+1],"yes") == 0) isothermal_flag = 1;
      else if (strcmp(arg[iarg+1],"no") == 0) isothermal_flag = 0;
      else error->all(FLERR,"Illegal collide command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"relaxtemp") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal collide command");
      if (strcmp(arg[iarg+1],"temp") == 0) relaxtemp = TRELAX;
      else if (strcmp(arg[iarg+1], "energy") == 0) relaxtemp = ECRELAX;
      else error->all(FLERR,"Illegal collide command");
      iarg += 2;
    } else error->all(FLERR,"Illegal collide command");
  }

  // proc 0 reads file to extract params for current species
  // broadcasts params to all procs

  nparams = particle->nspecies;
  if (nparams == 0)
    error->all(FLERR,"Cannot use collide command with no species defined");

  params = new Params[nparams];
  pairparams = new PairParams[nparams*nparams];
  spairparams = new PairParams[nparams*nparams];
  for (int i = 0; i < nparams*nparams; i++) {
    pairparams[i].diam = -1;
    spairparams[i].diam = -1;
  }

  if (comm->me == 0) read_param_file(arg[2]);
  MPI_Bcast(params,nparams*sizeof(Params),MPI_BYTE,0,world);
  MPI_Bcast(pairparams,nparams*nparams*sizeof(PairParams),MPI_BYTE,0,world);

  // check that params were read for all species

  for (int i = 0; i < nparams; i++)
    if (params[i].diam < 0.0) {
      char str[128];
      sprintf(str,"Species %s did not appear in VSS parameter file",
	      particle->species[i].id);
      error->one(FLERR,str);
    }

  // allocate per-species prefactor array
  iset_react_vss_pairparams = 0;
  memory->create(prefactor,nparams,nparams,"collide:prefactor");
}

/* ---------------------------------------------------------------------- */

CollideVSS::~CollideVSS()
{
  if (copymode) return;

  delete [] params;
  delete [] pairparams;
  delete [] spairparams;
  memory->destroy(prefactor);
}

/* ---------------------------------------------------------------------- */

void CollideVSS::init()
{
  // initially read-in per-species params must match current species list

  if (nparams != particle->nspecies)
    error->all(FLERR,"VSS parameters do not match current species");

  if (react) {
    int nreact_vss_pairparams = react->get_nreact_pair_params();
    if (nreact_vss_pairparams > 0 && !iset_react_vss_pairparams) {
      PairParams *p = react->get_react_vss_pairarams();
      for (int isp = 0; isp < nparams; isp++) {
        for (int jsp = isp; jsp < nparams; jsp++) {
          if (p[isp*nparams + jsp].diam > 0) {
            move_back_pair_params(isp, jsp, p[isp * nparams + jsp]);
          }
        }
      }
      iset_react_vss_pairparams = 1;
    }
  }

  Collide::init();

  if (relaxtemp == TRELAX && cell_temp_flag == 0) {
    error->all(FLERR, "CollideVSS is set with relaxtemp=temp"
      " but the method to calculate cell temperature is not found");
  }
}

/* ----------------------------------------------------------------------
   estimate a good value for vremax for a group pair in any grid cell
   called by Collide parent in init()
------------------------------------------------------------------------- */

double CollideVSS::vremax_init(int igroup, int jgroup)
{
  // parent has set mixture ptr

  Particle::Species *species = particle->species;
  double *vscale = mixture->vscale;
  int *mix2group = mixture->mix2group;
  int nspecies = particle->nspecies;

  double vrmgroup = 0.0;

  for (int isp = 0; isp < nspecies; isp++) {
    if (mix2group[isp] != igroup) continue;
    for (int jsp = 0; jsp < nspecies; jsp++) {
      if (mix2group[jsp] != jgroup) continue;

      double diam = 0.5 * (params[isp].diam + params[jsp].diam);
      double omega = 0.5 * (params[isp].omega + params[jsp].omega);
      double tref = 0.5 * (params[isp].tref + params[jsp].tref);
      double mr = species[isp].mass * species[jsp].mass /
	(species[isp].mass + species[jsp].mass);
      double cxs = diam*diam*MY_PI;
      prefactor[isp][jsp] = cxs *
	pow(2.0*update->boltz*tref/mr,omega-0.5)/tgamma(2.5-omega);
      double beta = MAX(vscale[isp],vscale[jsp]);
      double vrm = 2.0 * cxs * beta;
      if (pairparams2D(isp, jsp).diam > 0) {
        PairParams *p = &pairparams2D(isp, jsp);
        p->prefactor = p->diam * p->diam * MY_PI *
                       pow(2.0 * update->boltz * p->tref / mr, p->omega - 0.5) /
                       tgamma(2.5 - p->omega);
        vrm = 2.0 * p->diam * p->diam * MY_PI * beta;
      }
      vrmgroup = MAX(vrmgroup,vrm);
    }
  }

  return vrmgroup;
}

/* ---------------------------------------------------------------------- */

double CollideVSS::attempt_collision(int icell, int np, double volume)
{
 double fnum = update->fnum;
 double dt = update->dt;

 double nattempt;

 if (remainflag) {
   nattempt = 0.5 * np * (np-1) *
     vremax[icell][0][0] * dt * fnum / volume + remain[icell][0][0];
   remain[icell][0][0] = nattempt - static_cast<int> (nattempt);
 } else {
   nattempt = 0.5 * np * (np-1) *
     vremax[icell][0][0] * dt * fnum / volume + random->uniform();
 }

 return nattempt;
}

/* ---------------------------------------------------------------------- */

double CollideVSS::attempt_collision(int icell, int igroup, int jgroup,
				     double volume)
{
 double fnum = update->fnum;
 double dt = update->dt;

 double nattempt;

 // return 2x the value for igroup != jgroup, since no J,I pairing

 double npairs;
 if (igroup == jgroup) npairs = 0.5 * ngroup[igroup] * (ngroup[igroup]-1);
 else npairs = ngroup[igroup] * (ngroup[jgroup]);
 //else npairs = 0.5 * ngroup[igroup] * (ngroup[jgroup]);

 nattempt = npairs * vremax[icell][igroup][jgroup] * dt * fnum / volume;

 if (remainflag) {
   nattempt += remain[icell][igroup][jgroup];
   remain[icell][igroup][jgroup] = nattempt - static_cast<int> (nattempt);
 } else nattempt += random->uniform();

 return nattempt;
}

/* ----------------------------------------------------------------------
   determine if collision actually occurs
   1 = yes, 0 = no
   update vremax either way
------------------------------------------------------------------------- */

int CollideVSS::test_collision(int icell, int igroup, int jgroup,
			       Particle::OnePart *ip, Particle::OnePart *jp)
{
  double *vi = ip->v;
  double *vj = jp->v;
  int ispecies = ip->ispecies;
  int jspecies = jp->ispecies;
  double du  = vi[0] - vj[0];
  double dv  = vi[1] - vj[1];
  double dw  = vi[2] - vj[2];
  double vr2 = du*du + dv*dv + dw*dw;
  double omega1 = params[ispecies].omega;
  double omega2 = params[jspecies].omega;
  double omega = 0.5 * (omega1+omega2);
  PairParams *pp = &pairparams2D(ispecies, jspecies);
  if (pp->diam > 0) {
    omega = pp->omega;
  }
  double vro  = pow(vr2,1.0-omega);

  // although the vremax is calcualted for the group,
  // the individual collisions calculated species dependent vre
  double pf;
  if (pairparams2D(ispecies, jspecies).diam > 0) pf = pairparams2D(ispecies, jspecies).prefactor;
  else pf = prefactor[ispecies][jspecies];
  double vre = vro*pf;
  vremax[icell][igroup][jgroup] = MAX(vre,vremax[icell][igroup][jgroup]);
  if (vre/vremax[icell][igroup][jgroup] < random->uniform()) return 0;
  precoln.vr2 = vr2;
  return 1;
}

/* ---------------------------------------------------------------------- */

void CollideVSS::setup_collision(Particle::OnePart *ip, Particle::OnePart *jp)
{
  Particle::Species *species = particle->species;

  int isp = ip->ispecies;
  int jsp = jp->ispecies;

  precoln.vr = sqrt(precoln.vr2);

  precoln.ave_rotdof = 0.5 * (species[isp].rotdof + species[jsp].rotdof);
  precoln.ave_vibdof = 0.5 * (species[isp].vibdof + species[jsp].vibdof);
  precoln.ave_dof = (precoln.ave_rotdof  + precoln.ave_vibdof)/2.;

  double imass = precoln.imass = species[isp].mass;
  double jmass = precoln.jmass = species[jsp].mass;
  precoln.mr = imass * jmass / (imass+jmass);

  precoln.etrans = 0.5 * precoln.mr * precoln.vr2;
  precoln.erot = ip->erot + jp->erot;
  precoln.evib = ip->evib + jp->evib;

  precoln.eint   = precoln.erot + precoln.evib;
  precoln.etotal = precoln.etrans + precoln.eint;

  // COM velocity calculated using reactant masses

  double divisor = 1.0 / (imass+jmass);
  double *vi = ip->v;
  double *vj = jp->v;
  precoln.ucmf = ((imass*vi[0])+(jmass*vj[0])) * divisor;
  precoln.vcmf = ((imass*vi[1])+(jmass*vj[1])) * divisor;
  precoln.wcmf = ((imass*vi[2])+(jmass*vj[2])) * divisor;

  postcoln.etrans = precoln.etrans;
  postcoln.erot = 0.0;
  postcoln.evib = 0.0;
  postcoln.eint = 0.0;
  postcoln.etotal = precoln.etotal;
}

/* ---------------------------------------------------------------------- */

int CollideVSS::perform_collision(Particle::OnePart *&ip,
                                  Particle::OnePart *&jp,
                                  Particle::OnePart *&kp)
{
  int reactflag,kspecies;
  double x[3],v[3];
  Particle::OnePart *p3;

  // if gas-phase chemistry defined, attempt and perform reaction
  // if a 3rd particle is created, its kspecies >= 0 is returned
  // if 2nd particle is removed, its jspecies is set to -1

  if (react)
    reactflag = react->attempt(ip,jp,
                               precoln.etrans,precoln.erot,
                               precoln.evib,postcoln.etotal,kspecies);
  else reactflag = 0;

  // repartition energy and perform velocity scattering for I,J,K particles
  // reaction may have changed species of I,J particles
  // J,K particles may have been removed or created by reaction

  kp = NULL;

  if (reactflag && !react->frozenflag) {

    // add 3rd K particle if reaction created it
    // index of new K particle = nlocal-1
    // if add_particle() performs a realloc:
    //   make copy of x,v, then repoint ip,jp to new particles data struct

    if (kspecies >= 0) {
      int id = MAXSMALLINT*random->uniform();

      Particle::OnePart *particles = particle->particles;
      memcpy(x,ip->x,3*sizeof(double));
      memcpy(v,ip->v,3*sizeof(double));
      int reallocflag =
        particle->add_particle(id,kspecies,ip->icell,x,v,0.0,0.0);
      if (reallocflag) {
        ip = particle->particles + (ip - particles);
        jp = particle->particles + (jp - particles);
      }

      kp = &particle->particles[particle->nlocal-1];
      EEXCHANGE_ReactingEDisposal(ip,jp,kp);
      SCATTER_ThreeBodyScattering(ip,jp,kp);

    // remove 2nd J particle if recombination reaction removed it
    // p3 is 3rd particle participating in energy exchange

    } else if (jp->ispecies < 0) {
      double *vi = ip->v;
      double *vj = jp->v;

      double divisor = 1.0 / (precoln.imass + precoln.jmass);
      double ucmf = ((precoln.imass*vi[0]) + (precoln.jmass*vj[0])) * divisor;
      double vcmf = ((precoln.imass*vi[1]) + (precoln.jmass*vj[1])) * divisor;
      double wcmf = ((precoln.imass*vi[2]) + (precoln.jmass*vj[2])) * divisor;

      vi[0] = ucmf;
      vi[1] = vcmf;
      vi[2] = wcmf;

      jp = NULL;
      p3 = react->recomb_part3;

      // properly account for 3rd body energy with another call to setup_collision()
      // it needs relative velocity of recombined species and 3rd body

      double *vp3 = p3->v;
      double du  = vi[0] - vp3[0];
      double dv  = vi[1] - vp3[1];
      double dw  = vi[2] - vp3[2];
      double vr2 = du*du + dv*dv + dw*dw;
      precoln.vr2 = vr2;

      // internal energy of ip particle is already included
      //   in postcoln.etotal returned from react->attempt()
      // but still need to add 3rd body internal energy

      double partial_energy =  postcoln.etotal + p3->erot + p3->evib;

      ip->erot = 0;
      ip->evib = 0;
      p3->erot = 0;
      p3->evib = 0;

      // returned postcoln.etotal will increment only the
      //   relative translational energy between recombined species and 3rd body
      // add back partial_energy to get full total energy

      setup_collision(ip,p3);
      postcoln.etotal += partial_energy;

      if (precoln.ave_dof > 0.0) EEXCHANGE_ReactingEDisposal(ip,p3,jp);
      SCATTER_TwoBodyScattering(ip,p3);

    } else {
      EEXCHANGE_ReactingEDisposal(ip,jp,kp);
      SCATTER_TwoBodyScattering(ip,jp);
    }

  } else {
    if (precoln.ave_dof > 0.0) EEXCHANGE_NonReactingEDisposal(ip,jp);
    SCATTER_TwoBodyScattering(ip,jp);
  }

  return reactflag;
}

/* ---------------------------------------------------------------------- */

void CollideVSS::SCATTER_TwoBodyScattering(Particle::OnePart *ip,
					   Particle::OnePart *jp)
{
  double ua,vb,wc;
  double vrc[3];

  Particle::Species *species = particle->species;
  double *vi = ip->v;
  double *vj = jp->v;
  int isp = ip->ispecies;
  int jsp = jp->ispecies;
  double mass_i = species[isp].mass;
  double mass_j = species[jsp].mass;

  double alpha_r = 2.0 / (params[isp].alpha + params[jsp].alpha);
  PairParams *pp = &pairparams2D(isp, jsp);
  if (pp->diam > 0) {
    alpha_r = 1.0 / pp->alpha;
  }
  double mr = species[isp].mass * species[jsp].mass /
    (species[isp].mass + species[jsp].mass);

  if (check_scattering(isp, jsp, precoln.vr2, mr)) {
    double eps = random->uniform() * 2*MY_PI;
    if (fabs(alpha_r - 1.0) < 0.001) {
      double vr = sqrt(2.0 * postcoln.etrans / mr);
      double cosX = 2.0*random->uniform() - 1.0;
      double sinX = sqrt(1.0 - cosX*cosX);
      ua = vr*cosX;
      vb = vr*sinX*cos(eps);
      wc = vr*sinX*sin(eps);
    } else {
      double scale = sqrt((2.0 * postcoln.etrans) / (mr * precoln.vr2));
      double cosX = 2.0*pow(random->uniform(),alpha_r) - 1.0;
      double sinX = sqrt(1.0 - cosX*cosX);
      vrc[0] = vi[0]-vj[0];
      vrc[1] = vi[1]-vj[1];
      vrc[2] = vi[2]-vj[2];
      double d = sqrt(vrc[1]*vrc[1]+vrc[2]*vrc[2]);
      if (d > 1.0e-6) {
        ua = scale * ( cosX*vrc[0] + sinX*d*sin(eps) );
        vb = scale * ( cosX*vrc[1] + sinX*(precoln.vr*vrc[2]*cos(eps) -
                                          vrc[0]*vrc[1]*sin(eps))/d );
        wc = scale * ( cosX*vrc[2] - sinX*(precoln.vr*vrc[1]*cos(eps) +
                                          vrc[0]*vrc[2]*sin(eps))/d );
      } else {
        ua = scale * ( cosX*vrc[0] );
        vb = scale * ( sinX*vrc[0]*cos(eps) );
        wc = scale * ( sinX*vrc[0]*sin(eps) );
      }
    }
  } else {
    // For non-reaction collision
    //  - without VT/RT, this is equivalent to X = 0, no momentum exchange at all
    //  - with VT/RT, some momentum exchange occurs, but very small. Check Luo 2019 DSMC Talk
    double scale = sqrt((2.0 * postcoln.etrans) / (mr * precoln.vr2));
    ua = scale * (vi[0]-vj[0]);
    vb = scale * (vi[1]-vj[1]);
    wc = scale * (vi[2]-vj[2]);
  }

  // new velocities for the products

  double divisor = 1.0 / (mass_i + mass_j);
  vi[0] = precoln.ucmf + (mass_j*divisor)*ua;
  vi[1] = precoln.vcmf + (mass_j*divisor)*vb;
  vi[2] = precoln.wcmf + (mass_j*divisor)*wc;
  vj[0] = precoln.ucmf - (mass_i*divisor)*ua;
  vj[1] = precoln.vcmf - (mass_i*divisor)*vb;
  vj[2] = precoln.wcmf - (mass_i*divisor)*wc;
}

/* ---------------------------------------------------------------------- */

void CollideVSS::EEXCHANGE_NonReactingEDisposal(Particle::OnePart *ip,
						Particle::OnePart *jp)
{

  double State_prob,Fraction_Rot,Fraction_Vib,E_Dispose;
  int i,rotdof,vibdof,max_level,ivib,irot;

  Particle::OnePart *p;
  Particle::Species *species = particle->species;

  double AdjustFactor = 0.99999999;
  postcoln.erot = 0.0;
  postcoln.evib = 0.0;
  double pevib = 0.0;

  int icell = ip->icell;
  double *cell_temp;
  if (relaxtemp == TRELAX) {
    cell_temp = new double[ncell_temp];
    get_cell_temp(icell, cell_temp);
  }

  // handle each kind of energy disposal for non-reacting reactants

  if (precoln.ave_dof == 0) {
    ip->erot = 0.0;
    jp->erot = 0.0;
    ip->evib = 0.0;
    jp->evib = 0.0;

  } else {
    E_Dispose = precoln.etrans;

    double aveomega = 0.5*(params[ip->ispecies].omega +
                           params[jp->ispecies].omega);
    PairParams *pp = &pairparams2D(ip->ispecies, jp->ispecies);
    if (pp->diam > 0) {
      aveomega = pp->omega;
    }
    double transdof = 5.0 - 2.0 * aveomega;

    for (i = 0; i < 2; i++) {
      if (i == 0) p = ip;
      else p = jp;

      int sp = p->ispecies;
      rotdof = species[sp].rotdof;

      if (rotdof) {
        double rotn_phi = species[sp].rotrel;
        if (relaxflag == VARIABLE) {
          if (relaxtemp == TRELAX) rotn_phi = rotrel(sp,cell_temp);
          if (relaxtemp == ECRELAX) rotn_phi = rotrel(sp,aveomega,E_Dispose);
        }
        //  Haas's correction factor for RT: Eq (13) in doi:10.1063/1.868221
        rotn_phi *= 1.0 + rotdof/transdof;
        if (rotn_phi >= random->uniform()) {
          if (rotstyle == NONE) {
            p->erot = 0.0;
          } else if (rotstyle != NONE && rotdof == 2) {
            E_Dispose += p->erot;
            Fraction_Rot =
              1- pow(random->uniform(),(1/(2.5-aveomega)));
            p->erot = Fraction_Rot * E_Dispose;
            E_Dispose -= p->erot;
          } else {
            E_Dispose += p->erot;
            p->erot = E_Dispose *
              sample_bl(random,0.5*species[sp].rotdof-1.0,
                        1.5-aveomega);
            E_Dispose -= p->erot;
          }
        }
      }
      postcoln.erot += p->erot;

      vibdof = species[sp].vibdof;

      if (vibdof) {
        if (vibstyle == NONE) {
          p->evib = 0.0;
        } else {
          double vibn_phi = species[sp].vibrel[0];
          if (relaxflag == VARIABLE) {
            if (relaxtemp == TRELAX && vibdof == 2) vibn_phi = vibrel(sp,aveomega,cell_temp);
            if (relaxtemp == ECRELAX) vibn_phi = vibrel(sp,aveomega,E_Dispose+p->evib);
          }
          if (relaxtemp == TRELAX && vibdof == 2) {
            if (vibstyle == DISCRETE) {
              vibn_phi /= vibrel_sho_correct(sp, aveomega, cell_temp);
            }
          }
          if (vibn_phi >= random->uniform()) {
            if (vibdof == 2) {
              if (vibstyle == SMOOTH) {
                E_Dispose += p->evib;
                Fraction_Vib =
                  1.0 - pow(random->uniform(),(1.0/(2.5-aveomega)));
                p->evib= Fraction_Vib * E_Dispose;
                E_Dispose -= p->evib;

              } else if (vibstyle == DISCRETE) {
                E_Dispose += p->evib;
                if (species[sp].nvlevel == 0) {
                  max_level = static_cast<int>(
                      E_Dispose / (update->boltz * species[sp].vibtemp[0]));
                  do {
                    ivib = static_cast<int>(random->uniform() *
                                            (max_level + AdjustFactor));
                    p->evib = ivib * update->boltz * species[sp].vibtemp[0];
                    State_prob =
                        pow((1.0 - p->evib / E_Dispose), (1.5 - aveomega));
                  } while (State_prob < random->uniform());
                } else {
                  int m = species[sp].nvlevel;
                  double *vlevel = species[sp].vlevel;
                  // get last level not larger than E_Dispose
                  max_level = std::upper_bound(vlevel, vlevel + m, E_Dispose) -
                              vlevel - 1;
                  if (max_level == 0) {
                    p->evib = vlevel[0];
                  } else {
                    do {
                      ivib = static_cast<int>(random->uniform() *
                                              (max_level + AdjustFactor));
                      p->evib = vlevel[ivib];
                      State_prob =
                          pow((1.0 - p->evib / E_Dispose), (1.5 - aveomega));
                    } while (State_prob < random->uniform());
                  }
                }
                E_Dispose -= p->evib;
              }

            } else if (vibdof > 2) {
              if (vibstyle == SMOOTH) {
                E_Dispose += p->evib;
                p->evib = E_Dispose *
                  sample_bl(random,0.5*species[sp].vibdof-1.0,
                            1.5-aveomega);
                E_Dispose -= p->evib;

              } else if (vibstyle == DISCRETE) {
                p->evib = 0.0;

                int nmode = particle->species[sp].nvibmode;
                int **vibmode =
                  particle->eiarray[particle->ewhich[index_vibmode]];
                int pindex = p - particle->particles;

                for (int imode = 0; imode < nmode; imode++) {
                  ivib = vibmode[pindex][imode];
                  E_Dispose += ivib * update->boltz *
                    particle->species[sp].vibtemp[imode];
                  max_level = static_cast<int>
                    (E_Dispose / (update->boltz * species[sp].vibtemp[imode]));

                  do {
                    ivib = static_cast<int>
                      (random->uniform()*(max_level+AdjustFactor));
                    pevib = ivib * update->boltz * species[sp].vibtemp[imode];
                    State_prob = pow((1.0 - pevib / E_Dispose),
                                    (1.5 - aveomega));
                  } while (State_prob < random->uniform());

                  vibmode[pindex][imode] = ivib;
                  p->evib += pevib;
                  E_Dispose -= pevib;
                }
              }
            } // end of vibstyle/vibdof if
          }
        }
        postcoln.evib += p->evib;
      } // end of vibdof if
    }
  }

  // compute portion of energy left over for scattering

  postcoln.eint = postcoln.erot + postcoln.evib;
  if (isothermal_flag == 0) postcoln.etrans = E_Dispose;
  if (relaxtemp == TRELAX) {
    delete [] cell_temp;
  }
}

/* ---------------------------------------------------------------------- */

void CollideVSS::SCATTER_ThreeBodyScattering(Particle::OnePart *ip,
			  		     Particle::OnePart *jp,
			  		     Particle::OnePart *kp)
{
  double vrc[3],ua,vb,wc;

  Particle::Species *species = particle->species;
  int isp = ip->ispecies;
  int jsp = jp->ispecies;
  int ksp = kp->ispecies;
  double mass_i = species[isp].mass;
  double mass_j = species[jsp].mass;
  double mass_k = species[ksp].mass;
  double mass_ij = mass_i + mass_j;
  double *vi = ip->v;
  double *vj = jp->v;
  double *vk = kp->v;

  double alpha_r = 2.0 / (params[isp].alpha + params[jsp].alpha);
  PairParams *pp = &pairparams2D(isp, jsp);
  if (pp->diam > 0) {
    alpha_r = 1.0 / pp->alpha;
  }
  double mr = mass_ij * mass_k / (mass_ij + mass_k);
  postcoln.eint = ip->erot + jp->erot + ip->evib + jp->evib
                + kp->erot + kp->evib;

  if (check_scattering(isp, jsp, precoln.vr2, mr)) {
    double cosX = 2.0*pow(random->uniform(), alpha_r) - 1.0;
    double sinX = sqrt(1.0 - cosX*cosX);
    double eps = random->uniform() * 2*MY_PI;

    if (fabs(alpha_r - 1.0) < 0.001) {
      double vr = sqrt(2*postcoln.etrans/mr);
      ua = vr*cosX;
      vb = vr*sinX*cos(eps);
      wc = vr*sinX*sin(eps);
    } else {
      double scale = sqrt((2.0*postcoln.etrans) / (mr*precoln.vr2));
      vrc[0] = vi[0]-vj[0];
      vrc[1] = vi[1]-vj[1];
      vrc[2] = vi[2]-vj[2];
      double d = sqrt(vrc[1]*vrc[1]+vrc[2]*vrc[2]);
      if (d > 1.E-6 ) {
        ua = scale * (cosX*vrc[0] + sinX*d*sin(eps));
        vb = scale * (cosX*vrc[1] + sinX*(precoln.vr*vrc[2]*cos(eps) -
                                          vrc[0]*vrc[1]*sin(eps))/d);
        wc = scale * (cosX*vrc[2] - sinX*(precoln.vr*vrc[1]*cos(eps) +
                                          vrc[0]*vrc[2]*sin(eps))/d);
      } else {
        ua = scale * cosX*vrc[0];
        vb = scale * sinX*vrc[0]*cos(eps);
        wc = scale * sinX*vrc[0]*sin(eps);
      }
    }
  } else {
    double scale = sqrt((2.0*postcoln.etrans) / (mr*precoln.vr2));
    ua = scale * (vi[0]-vj[0]);
    vb = scale * (vi[1]-vj[1]);
    wc = scale * (vi[2]-vj[2]);
  }

  // new velocities for the products

  double divisor = 1.0 / (mass_ij + mass_k);
  vi[0] = precoln.ucmf + (mass_k*divisor)*ua;
  vi[1] = precoln.vcmf + (mass_k*divisor)*vb;
  vi[2] = precoln.wcmf + (mass_k*divisor)*wc;
  vk[0] = precoln.ucmf - (mass_ij*divisor)*ua;
  vk[1] = precoln.vcmf - (mass_ij*divisor)*vb;
  vk[2] = precoln.wcmf - (mass_ij*divisor)*wc;
  vj[0] = vi[0];
  vj[1] = vi[1];
  vj[2] = vi[2];
}

/* ---------------------------------------------------------------------- */

void CollideVSS::EEXCHANGE_ReactingEDisposal(Particle::OnePart *ip,
                                             Particle::OnePart *jp,
                                             Particle::OnePart *kp)
{
  double State_prob,Fraction_Rot,Fraction_Vib;
  int i,numspecies,rotdof,vibdof,max_level,ivib,irot;
  double aveomega,pevib;

  Particle::OnePart *p;
  Particle::Species *species = particle->species;
  double AdjustFactor = 0.99999999;

  if (!kp) {
    ip->erot = 0.0;
    jp->erot = 0.0;
    ip->evib = 0.0;
    jp->evib = 0.0;
    numspecies = 2;
    aveomega = 0.5*(params[ip->ispecies].omega + params[jp->ispecies].omega);
    PairParams *pp = &pairparams2D(ip->ispecies, jp->ispecies);
    if (pp->diam > 0) {
      aveomega = pp->omega;
    }
  } else {
    ip->erot = 0.0;
    jp->erot = 0.0;
    kp->erot = 0.0;
    ip->evib = 0.0;
    jp->evib = 0.0;
    kp->evib = 0.0;
    numspecies = 3;
    aveomega = (params[ip->ispecies].omega + params[jp->ispecies].omega +
                params[kp->ispecies].omega)/3;
  }

  // handle each kind of energy disposal for non-reacting reactants
  // clean up memory for the products

  double E_Dispose = postcoln.etotal;

  for (i = 0; i < numspecies; i++) {
    if (i == 0) p = ip;
    else if (i == 1) p = jp;
    else p = kp;

    int sp = p->ispecies;
    rotdof = species[sp].rotdof;

    if (rotdof) {
      if (rotstyle == NONE) {
        p->erot = 0.0;
      } else if (rotdof == 2) {
        Fraction_Rot =
          1- pow(random->uniform(),(1/(2.5-aveomega)));
        p->erot = Fraction_Rot * E_Dispose;
        E_Dispose -= p->erot;

      } else if (rotdof > 2) {
        p->erot = E_Dispose *
          sample_bl(random,0.5*species[sp].rotdof-1.0,
                    1.5-aveomega);
        E_Dispose -= p->erot;
      }
    }

    vibdof = species[sp].vibdof;

    if (vibdof) {
      if (vibstyle == NONE) {
        p->evib = 0.0;
      } else if (vibdof == 2 && vibstyle == DISCRETE) {
        if (species[sp].nvlevel == 0) {
          max_level = static_cast<int>(
              E_Dispose / (update->boltz * species[sp].vibtemp[0]));
          do {
            ivib = static_cast<int>(random->uniform() *
                                    (max_level + AdjustFactor));
            p->evib = (double)(ivib * update->boltz * species[sp].vibtemp[0]);
            State_prob = pow((1.0 - p->evib / E_Dispose), (1.5 - aveomega));
          } while (State_prob < random->uniform());
        } else {
          int m = species[sp].nvlevel;
          double *vlevel = species[sp].vlevel;
          // get last level not larger than E_Dispose
          max_level =
              std::upper_bound(vlevel, vlevel + m, E_Dispose) - vlevel - 1;
          if (max_level == 0) {
            p->evib = vlevel[0];
          } else {
            do {
              ivib = static_cast<int>(random->uniform() *
                                      (max_level + AdjustFactor));
              p->evib = vlevel[ivib];
              State_prob = pow((1.0 - p->evib / E_Dispose), (1.5 - aveomega));
            } while (State_prob < random->uniform());
          }
        }
        E_Dispose -= p->evib;

      } else if (vibdof == 2 && vibstyle == SMOOTH) {
        Fraction_Vib =
          1.0 - pow(random->uniform(),(1.0 / (2.5-aveomega)));
        p->evib = Fraction_Vib * E_Dispose;
        E_Dispose -= p->evib;

      } else if (vibdof > 2 && vibstyle == SMOOTH) {
          p->evib = E_Dispose *
          sample_bl(random,0.5*species[sp].vibdof-1.0,
                   1.5-aveomega);
          E_Dispose -= p->evib;
      } else if (vibdof > 2 && vibstyle == DISCRETE) {
          p->evib = 0.0;

          int nmode = particle->species[sp].nvibmode;
          int **vibmode = particle->eiarray[particle->ewhich[index_vibmode]];
          int pindex = p - particle->particles;

          for (int imode = 0; imode < nmode; imode++) {
            ivib = vibmode[pindex][imode];
            E_Dispose += ivib * update->boltz *
            particle->species[sp].vibtemp[imode];
            max_level = static_cast<int>
            (E_Dispose / (update->boltz * species[sp].vibtemp[imode]));
            do {
              ivib = static_cast<int>
              (random->uniform()*(max_level+AdjustFactor));
              pevib = ivib * update->boltz * species[sp].vibtemp[imode];
              State_prob = pow((1.0 - pevib / E_Dispose),
                               (1.5 - aveomega));
            } while (State_prob < random->uniform());

            vibmode[pindex][imode] = ivib;
            p->evib += pevib;
            E_Dispose -= pevib;
          }
        }
      }
    }

  // compute post-collision internal energies

  postcoln.erot = ip->erot + jp->erot;
  postcoln.evib = ip->evib + jp->evib;

  if (kp) {
    postcoln.erot += kp->erot;
    postcoln.evib += kp->evib;
  }

  // compute portion of energy left over for scattering

  postcoln.eint = postcoln.erot + postcoln.evib;
  if (isothermal_flag == 0) postcoln.etrans = E_Dispose;
}

/* ---------------------------------------------------------------------- */

double CollideVSS::sample_bl(RanPark *random, double Exp_1, double Exp_2)
{
  double Exp_s = Exp_1 + Exp_2;
  double x,y;
  do {
    x = random->uniform();
    y = pow(x*Exp_s/Exp_1, Exp_1)*pow((1.0-x)*Exp_s/Exp_2, Exp_2);
  } while (y < random->uniform());
  return x;
}

/* ----------------------------------------------------------------------
   compute a variable rotational relaxation parameter
------------------------------------------------------------------------- */

double CollideVSS::rotrel(int isp, double aveomega, double Ec)
{
  double Tr = Ec /(update->boltz * (2.5-aveomega));
  double rotphi = (1.0+params[isp].rotc2/sqrt(Tr) + params[isp].rotc3/Tr)
                / params[isp].rotc1;
  return rotphi;
}

double CollideVSS::rotrel(int isp, double *cell_temp)
{
  double Tt = cell_temp[0];
  double rotphi = (1.0+params[isp].rotc2/sqrt(Tt) + params[isp].rotc3/Tt)
                / params[isp].rotc1;
  return rotphi;
}

/* ----------------------------------------------------------------------
   compute a variable vibrational relaxation parameter
------------------------------------------------------------------------- */

double CollideVSS::vibrel(int isp, double aveomega, double Ec)
{
  double Tr = Ec /(update->boltz * (3.5-aveomega));
  double omega = aveomega;
  double vibphi = 1.0 / (params[isp].vibc1/pow(Tr,omega) *
                         exp(params[isp].vibc2/pow(Tr,1.0/3.0)));
  return vibphi;
}

double CollideVSS::vibrel(int isp, double aveomega, double *cell_temp)
{
  double Tt = cell_temp[0];
  // aveomega is used here because the collision rate is propotional to
  // pow(Tt, 1-aveomgea). Check Bird's 1994 Book page 141.
  double vibphi = 1.0 / (params[isp].vibc1/pow(Tt,aveomega) *
                         exp(params[isp].vibc2/pow(Tt,1.0/3.0)));
  return vibphi;
}

/* ----------------------------------------------------------------------
   compute a correction factor for VT relaxation number with SHO model
   theory comes from Gimelshein 2002 doi:10.1063/1.1517297
------------------------------------------------------------------------- */

double CollideVSS::vibrel_sho_correct(int isp, double aveomega, double *cell_temp)
{
  double Tt = cell_temp[0];
  double Tv = cell_temp[2];
  if (ncell_temp > 2)  Tv = cell_temp[2+isp];
  double transdof = 5.0 - 2.0 * aveomega;
  double theta_v = particle->species[isp].vibtemp[0];
  double x[2], zeta_v[2];
  x[0] = theta_v / Tt;
  zeta_v[0] = 2.0 * x[0] / (exp(x[0]) - 1.0);    // zeta(Tt)
  if (Tv > 1e-3 * theta_v) {
    x[1] = theta_v / Tv;
    zeta_v[1] = 2.0 * x[1] / (exp(x[1]) - 1.0);  // zeta(Tv)
  } else {
    zeta_v[1] = 0.0;
  }

  double cref = 1.0;
  if (Tv > Tt) {
    double lhs = zeta_v[1] * Tv + transdof * Tt;
    auto f2solve = [transdof, lhs, theta_v](double Tp) {
      double xp = theta_v / Tp;
      double zeta_vp = 2.0 * xp / (exp(xp) - 1.0);
      return lhs - (transdof + zeta_vp) * Tp;
    };
    double Tp = MathExtra::root_finding_brents(f2solve, Tt, Tv, 0.01);
    double err = f2solve(Tp);
    if (!isnan(Tp)) {
      cref = transdof * (Tt - Tp) / (zeta_v[0] * Tt - zeta_v[1] * Tv);
    } else {
      char str[128];
      sprintf(
          str,
          "CollideVss::vibrel_serial root_finding_brents fail to converge for "
          "thetaV=%.2ff Tt=%.2f Tv=%.2f",
          theta_v, Tt, Tv);
      error->warning(FLERR, str);
    }
  } else {
    double A = 0.5 * zeta_v[0] * zeta_v[0] * exp(x[0]);
    cref = transdof / (transdof + A);
  }
  return cref;
}

/* ----------------------------------------------------------------------
   read list of species defined in species file
   store info in filespecies and nfilespecies
   only invoked by proc 0
------------------------------------------------------------------------- */

void CollideVSS::read_param_file(char *fname)
{
  FILE *fp = fopen(fname,"r");
  if (fp == NULL) {
    char str[128];
    sprintf(str,"Cannot open VSS parameter file %s",fname);
    error->one(FLERR,str);
  }

  // set all diameters to -1, so can detect if not read

  for (int i = 0; i < nparams; i++) params[i].diam = -1.0;
  for (int i = 0; i < nparams*nparams; i++) pairparams[i].diam = -1.0;

  // read file line by line
  // skip blank lines or comment lines starting with '#'
  // all other lines must have at least NWORDS

  int NWORDS = 5;
  if (relaxflag == VARIABLE) NWORDS = 9;
  char **words = new char*[NWORDS];
  char line[MAXLINE],copy[MAXLINE];
  int isp;

  while (fgets(line,MAXLINE,fp)) {
    int pre = strspn(line," \t\n\r");
    if (pre == strlen(line) || line[pre] == '#') continue;

    strcpy(copy,line);
    int nwords = wordcount(copy);
    if (nwords == 6) {
      // VHS pair data
      wordparse(nwords,line,words);
      isp = particle->find_species(words[0]);
      int jsp = particle->find_species(words[1]);
      if (isp < 0 || jsp < 0) continue;
      PairParams *pp = &pairparams2D(isp, jsp);
      pp->diam = atof(words[2]);
      pp->omega = atof(words[3]);
      pp->tref = atof(words[4]);
      pp->alpha = atof(words[5]);
    } else {
      if (nwords < NWORDS)
        error->one(FLERR,"Incorrect line format in VSS parameter file");
      wordparse(NWORDS,line,words);

      isp = particle->find_species(words[0]);
      if (isp < 0) continue;

      params[isp].diam = atof(words[1]);
      params[isp].omega = atof(words[2]);
      params[isp].tref = atof(words[3]);
      params[isp].alpha = atof(words[4]);
      if (relaxflag == VARIABLE) {
        params[isp].rotc1 = atof(words[5]);
        params[isp].rotc2 = atof(words[6]);
        params[isp].rotc3 =  (MY_PI+MY_PI2*MY_PI2)*params[isp].rotc2;
        params[isp].rotc2 =  (MY_PI*MY_PIS/2.)*sqrt(params[isp].rotc2);
        params[isp].vibc1 = atof(words[7]);
        params[isp].vibc2 = atof(words[8]);
      }
    }
  }

  delete [] words;
  fclose(fp);
}

/* ----------------------------------------------------------------------
   count whitespace-delimited words in line
------------------------------------------------------------------------- */

int CollideVSS::wordcount(char *line)
{
  int nwords = 0;
  char *word = strtok(line," \t");
  while (word) {
    nwords++;
    word = strtok(NULL," \t");
  }
  return nwords;
}

/* ----------------------------------------------------------------------
   parse first N whitespace-delimited words in line
   store ptr to each word in words
------------------------------------------------------------------------- */

void CollideVSS::wordparse(int n, char *line, char **words)
{
  for (int i = 0; i < n; i++) {
    if (i == 0) words[i] = strtok(line," \t");
    else words[i] = strtok(NULL," \t");
  }
}

/* ----------------------------------------------------------------------
   return a per-species parameter to caller
------------------------------------------------------------------------- */

double CollideVSS::extract(int isp, const char *name)
{
  if (strcmp(name,"diam") == 0) return params[isp].diam;
  else if (strcmp(name,"omega") == 0) return params[isp].omega;
  else if (strcmp(name,"tref") == 0) return params[isp].tref;
  else error->all(FLERR,"Request for unknown parameter from collide");
  return 0.0;
}

/* ----------------------------------------------------------------------
   return a pair parameter to caller
   If no pair parameters exist, return the information of the first
   specie
------------------------------------------------------------------------- */

double CollideVSS::extract(int isp, int jsp, const char *name)
{
  PairParams *pp = &pairparams2D(isp, jsp);
  if (pp->diam > 0) {
    if (strcmp(name,"diam") == 0) return pp->diam;
    else if (strcmp(name,"omega") == 0) return pp->omega;
    else if (strcmp(name,"tref") == 0) return pp->tref;
    else error->all(FLERR,"Request for unknown parameter from collide");
    return 0.0;
  } else {
    return this->extract(isp, name);
  }
}

/* ----------------------------------------------------------------------
   Set pairparams or spairparams

    If a bare struct is passed as a parameter, it is copied and the copy is passed.
    If a bare struct is returned from a function, a copy is made and the copy is returned.
    If a bare struct appears on both sides of an assignment operator, a copy of the struct
       on the right is made and copied into the storage for the struct on the left.
       Pointers inside structs are copied, but what they point to is not copied,
       that is, after the copy, the pointer in the original and in the copy will
       point to the same object.
------------------------------------------------------------------------- */

SPARTA_NS::CollideVSS::PairParams CollideVSS::get_pair_params(int isp, int jsp, int itype) {
  if (itype == 0) {
    if (pairparams2D(isp, jsp).diam < 0){
      PairParams result;
      result.diam = 0.5 * (params[isp].diam + params[jsp].diam);
      result.omega = 0.5 * (params[isp].omega + params[jsp].omega);
      result.tref = 0.5 * (params[isp].tref + params[jsp].tref);
      result.alpha = 0.5* (params[isp].alpha + params[jsp].alpha);
      return result;
    } else {
      return pairparams2D(isp, jsp);
    }
  } else if (itype == 1) {
    return pairparams2D(isp, jsp);
  } else {
    return spairparams2D(isp, jsp);
  }
}


/* ----------------------------------------------------------------------
   Set pairparams or spairparams
------------------------------------------------------------------------- */

void CollideVSS::set_pair_params(int isp, int jsp, int itype, PairParams newparam) {
  if (itype == 0) pairparams2D(isp, jsp) = newparam;
  else spairparams2D(isp, jsp) = newparam;
}

/* ----------------------------------------------------------------------
   Move value of pairparams2D(isp, jsp) to spairparams2D(isp, jsp)
   and set pairparams2D(isp, jsp) to the new value
------------------------------------------------------------------------- */

void CollideVSS::move_back_pair_params(int isp, int jsp, PairParams newparam) {
    spairparams2D(isp, jsp)= get_pair_params(isp, jsp, 0);
    pairparams2D(isp, jsp) = newparam;
}

/* ----------------------------------------------------------------------
   Move value of spairparams2D(isp, jsp) to pairparams2D(isp, jsp)
   and clear spairparams2D(isp, jsp)
------------------------------------------------------------------------- */

void CollideVSS::move_forward_pair_params(int isp, int jsp) {
  if (spairparams2D(isp, jsp).diam < 0) {
    pairparams2D(isp, jsp).diam = -1; // use average value
  } else {
    pairparams2D(isp, jsp) = spairparams2D(isp, jsp);
  }
  spairparams2D(isp, jsp).diam = -1;
}

/* ----------------------------------------------------------------------
   check whether scattering probability based on pairparams and spairparams
------------------------------------------------------------------------- */

int CollideVSS::check_scattering(int isp, int jsp, double vr2, double mr) {
  if (spairparams2D(isp, jsp).diam < 0) return 1;

  Particle::Species *species = particle->species;
  if (mr < 0)
    mr = species[isp].mass * species[jsp].mass /
         (species[isp].mass + species[jsp].mass);

  double dsqure[2];
  for (int i = 0; i < 2; i++) {
    PairParams p = this->get_pair_params(isp, jsp);
    if (i == 1) p = spairparams2D(isp, jsp);
    dsqure[i] = p.diam*p.diam*pow(2.0*update->boltz*p.tref/mr/vr2  , p.omega-0.5)/tgamma(2.5-p.omega);
  }
  if (dsqure[1] > dsqure[0]) return 1;
  if (random->uniform() < dsqure[1]/dsqure[0]) return 1;
  return 0;
}


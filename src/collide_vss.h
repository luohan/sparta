/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#ifdef COLLIDE_CLASS

CollideStyle(vss,CollideVSS)

#else

#ifndef SPARTA_COLLIDE_VSS_H
#define SPARTA_COLLIDE_VSS_H

#include "collide.h"
#include "particle.h"

namespace SPARTA_NS {

class CollideVSS : public Collide {
 public:
  CollideVSS(class SPARTA *, int, char **);
  virtual ~CollideVSS();
  virtual void init();

  double vremax_init(int, int);
  virtual double attempt_collision(int, int, double);
  double attempt_collision(int, int, int, double);
  virtual int test_collision(int, int, int, Particle::OnePart *, Particle::OnePart *);
  virtual void setup_collision(Particle::OnePart *, Particle::OnePart *);
  virtual int perform_collision(Particle::OnePart *&, Particle::OnePart *&,
                        Particle::OnePart *&);
  double extract(int, const char *);
  double extract(int, int, const char *);

  struct State {      // two-particle state
    double vr2;
    double vr;
    double imass,jmass;
    double mr;
    double ave_rotdof;
    double ave_vibdof;
    double ave_dof;
    double etrans;
    double erot;
    double evib;
    double eexchange;
    double eint;
    double etotal;
    double ucmf;
    double vcmf;
    double wcmf;
  };

  struct Params {             // VSS model parameters
    double diam;
    double omega;
    double tref;
    double alpha;
    double rotc1;             // Zrotinf (unitless)
    double rotc2;             // T* (temperature units)
    double rotc3;             // C1 (temperature units)
    double vibc1;             // C1 (temperature units)
    double vibc2;             // C2 (temperature units)
  };

  struct PairParams {         // VSS model parameters for pair
    double diam;
    double omega;
    double tref;
    double alpha;
    double prefactor;
  };
  PairParams get_pair_params(int, int, int=0);
  void set_pair_params(int, int, int, PairParams);
  void move_back_pair_params(int, int, PairParams);
  void move_forward_pair_params(int, int);
  static int wordcount(char *);
  static void wordparse(int, char *, char **);

 protected:
  int relaxflag,eng_exchange,isothermal_flag,relaxtemp;
  double vr_indice;
  double **prefactor; // static portion of collision attempt frequency

  struct State precoln;       // state before collision
  struct State postcoln;      // state after collision

  Params *params;             // VSS params for each species
  int nparams;                // # of per-species params read in

  PairParams *pairparams;         // VSS params defined for pairs to calculate
                                  // total collision cross sections
                                  // if .diam = - 1, use average of `params`
  PairParams *spairparams;  // VSS params defined for pairs to calculate
                                  // scattering collision cross sections
                                 // if .diam = - 1, scattering = total
                            // set by CollideVSS::move_back_pair_params
                            // should be set in CollideVSS::init
                            // values are obtained from react::react_vss_pairparams
  int iset_react_vss_pairparams;     // prevent reset between runs

  int check_scattering(int, int, double, double=-1);   // output a scattering probability based on pairparams and spairparams

  void SCATTER_TwoBodyScattering(Particle::OnePart *,
				 Particle::OnePart *);
  void EEXCHANGE_NonReactingEDisposal(Particle::OnePart *,
				      Particle::OnePart *);
  void SCATTER_ThreeBodyScattering(Particle::OnePart *,
                                   Particle::OnePart *,
                                   Particle::OnePart *);
  void EEXCHANGE_ReactingEDisposal(Particle::OnePart *,
                                   Particle::OnePart *,
                                   Particle::OnePart *);

  double sample_bl(RanPark *, double, double);
  double rotrel (int, double, double);      // RT relaxation probability calculated with collisional temperature
  double rotrel (int, double *);  // RT relaxation probability calculated with cell translational temperature
  double vibrel (int, double, double);      // VT relaxation probability calculated with collisional temperature
  double vibrel (int, double, double *);  // VT relaxation probability calculated with cell translational temperature
  double vibrel_sho_correct (int, double, double *);

  void read_param_file(char *);
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running SPARTA to see the offending line.

E: Species %s did not appear in VSS parameter file

Self-explanatory.

E: VSS parameters do not match current species

Species cannot be added after VSS colision file is read.

E: Cannot open VSS parameter file %s

Self-explanatory.

E: Incorrect line format in VSS parameter file

Number of parameters in a line read from file is not valid.

E: Request for unknown parameter from collide

VSS model does not have the parameter being requested.

*/

/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#ifdef REACT_CLASS

ReactStyle(tce/mf,ReactTCEMF)

#else

#ifndef SPARTA_REACT_TCE_MF_H
#define SPARTA_REACT_TCE_MF_H

#ifndef SPARTA_REACT_TCE_MF_STRLEN
#define SPARTA_REACT_TCE_MF_STRLEN 16
#endif

#define MF_DD_NANGLE 9
#define MF_AD_NANGLE 4

#include "react_bird.h"
#include "particle.h"
#include "collide_vss.h"

namespace SPARTA_NS {

class ReactTCEMF : public ReactBird {
 public:
  ReactTCEMF(class SPARTA *, int, char **);
  ~ReactTCEMF();
  void init();
  int attempt(Particle::OnePart *, Particle::OnePart *,
              double, double, double, double &, int &);
  void modify_params(int, char **);

 private:
  // data type to store MF model parameters
  struct OneSpecieMF {
     int id;  // id of particle->species
     char name[SPARTA_REACT_TCE_MF_STRLEN];   // name of  dissociating particle

     int npartner;   // number of possible colliding partners
     int *id_partner;
     char **name_partner; // a list of colliding partnet

     int iaho;
     // iaho = 0 : SHO phase
     //      = 1 : AHO phase calculated using Morse parameters
     // Morse potential V(r) = De*(1-exp(-alpha*(r-re)))^2 - De
     // This V(r) will be zero at infinity and -De at zero-point
     // We need a zero point energy (De - D0) since in DSMC Ev(v=0) = 0
     double De;      // Potential valley of your the Morse potential n eV
     double re;      // equilibrium distance in angstrom
     double alpha;   // alpha in angstrom^-1
     double D0;      // Dissociation energy/Potential valley of your AHO ladder in eV
     double Ev0;     // Ev(v=0) zeropoint energy should be larger than 0
     bool operator==(const OneSpecieMF &rhs) const
     {
        return this->id == rhs.id;
     }
  };

  struct OneReactionMF {
     int nangle;            // -1:  it's not a dissociation reaction
                            // 0:  not TCE corresponding MF but it is a dissociation
                            // 4:  atom-diatom
                            // 9:  diatom-diatom
     int double_diss;       // 0:  if only AB+CD=>A+B+CD dissociation is defined
                            // 1:  if both AB and CD can be dissociated
                            // note: no double dissociation
     OneReactionMF *sec;      // second reaction channel AB+CD and CD+AB dissociation
     OneSpecieMF *ispmf;
     OneSpecieMF *jspmf;    // NULL if nangle = 4
     int ispa, ispb, ispc, ispd; // id of atoms
     double factor[8];
     bool operator==(const OneReactionMF &rhs) const
     {
        return (this->jspmf == rhs.jspmf && this->ispmf == rhs.ispmf) ||
               (this->jspmf == rhs.ispmf && this->ispmf == rhs.jspmf);
     }
  };

  OneSpecieMF *splist_mf;
  OneReactionMF *rlist_mf;  // corresponds to ReactBird:rlist
  int nsp_mf;

//   int attempt_tce(Particle::OnePart *, Particle::OnePart *, OneReaction *,
//                   double, double, double, double &, int &);
//   int attempt_mf(Particle::OnePart *, Particle::OnePart *, OneReaction *, OneReactionMF *,
//                  double, double, double, double &, int &);
  int readfile_mf(char *);
  int readfile_mfvss(char *);

  bool called;
  void sample_phase(int, double, double);
  void sample_phase(int, double);
  double sample_vibration_phase(OneSpecieMF *, double);
  double fgamma_1,fgamma_2,ftheta,fphi_0,fphi_1,fbeta_1,fbeta_2,fdelta,fbeta;
   // storage of the phase angle
   // mfang
   //  0. gamma_1
   //  1. gamma_2
   //  2. theta
   //  3. phi_0
   //  4. phi_1
   //  5. beta_1
   //  6. beta_2
   //  7. delta
   //  8. beta -> this angle doesn't contribute to AB dissociation
  double  evaluate_threshold(int, double, double, double, double);
  double  evaluate_threshold(int, double, double);

};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running SPARTA to see the offending line.

E: React tce can only be used with collide vss

Self-explanatory.

E: Ionization and recombination reactions are not yet implemented

This error conditions will be removed after those reaction styles are
fully implemented.

E: Unknown outcome in reaction

The specified type of the reaction is not encoded in the reaction
style.

E: Cannot open reaction file %s

Self-explanatory.

E: Invalid reaction formula in file

Self-explanatory.

E: Invalid reaction type in file

Self-explanatory.

E: Invalid reaction style in file

Self-explanatory.

E: Invalid reaction coefficients in file

Self-explanatory.

*/

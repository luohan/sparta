/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "spatype.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "math_extra.h"
#include <algorithm>
#include <cmath>

using namespace SPARTA_NS;

namespace MathExtra {

/* ----------------------------------------------------------------------
   compute bounds implied by numeric str with a possible wildcard asterik
   1 = lower bound, Nmax = upper bound
   5 possibilities:
     (1) i = i to i, (2) * = 1 to Nmax,
     (3) i* = i to Nmax, (4) *j = 1 to j, (5) i*j = i to j
   return nlo,nhi
   return 0 if successful
   return 1 if numeric values are out of lower/upper bounds
------------------------------------------------------------------------- */

int bounds(char *str, int nmax, int &nlo, int &nhi)
{
  char *ptr = strchr(str,'*');

  if (ptr == NULL) {
    nlo = nhi = atoi(str);
  } else if (strlen(str) == 1) {
    nlo = 1;
    nhi = nmax;
  } else if (ptr == str) {
    nlo = 1;
    nhi = atoi(ptr+1);
  } else if (strlen(ptr+1) == 0) {
    nlo = atoi(str);
    nhi = nmax;
  } else {
    nlo = atoi(str);
    nhi = atoi(ptr+1);
  }

  if (nlo < 1 || nhi > nmax) return 1;
  return 0;
}

/* ----------------------------------------------------------------------
   convert a 64-bit integer into a string like "9.36B"
   K = thousand, M = million, B = billion, T = trillion, P = peta, E = exa
   for easier-to-understand output
------------------------------------------------------------------------- */

char *num2str(bigint n, char *outstr)
{
  if (n < 100000) sprintf(outstr,"(%1.3gK)",1.0e-3*n);
  else if (n < 1000000000) sprintf(outstr,"(%1.3gM)",1.0e-6*n);
  else if (n < 1000000000000) sprintf(outstr,"(%1.3gB)",1.0e-9*n);
  else if (n < 1000000000000000) sprintf(outstr,"(%1.3gT)",1.0e-12*n);
  else if (n < 1000000000000000000) sprintf(outstr,"(%1.3gP)",1.0e-15*n);
  else sprintf(outstr,"(%1.3gE)",1.0e-18*n);
  return outstr;
}

/* ----------------------------------------------------------------------
  root finding based on Brent's method
  f: function to solve, it can be
     1. a pure function: double (*f)(double)
     2. a functor with operator () (double x) { return double}
     3. a lambda expression like: [=](double x){return double}
     4. a std::bind
  lower, upper: lower and upper limit of the range of solution
  tol: tolerance defined as abs(xleft - xright). If tol < 0, it is set automatically
  max_iter: maximum number of iteration
------------------------------------------------------------------------- */

double root_finding_brents(std::function<double (double)> f, double lower, double upper, double tol, unsigned int max_iter)
{
	double a = lower, b = upper;
	double fa = f(a), fb = f(b);	// calculated now to save function calls
	double fs = 0;		// initialize

  if (tol < 0) tol = 1e-6 * abs(b); // 1-x1/x2 = 1e-6

	if (!(fa * fb < 0)) return NAN;

	if (abs(fa) < abs(b))	// if magnitude of f(lower_bound) is less than magnitude of f(upper_bound)
	{
		std::swap(a,b);
		std::swap(fa,fb);
	}

	double c = a;			// c now equals the largest magnitude of the lower and upper bounds
	double fc = fa;			// precompute function evalutation for point c by assigning it the same value as fa
	bool mflag = true;		// boolean flag used to evaluate if statement later on
	double s = 0;			// Our Root that will be returned
	double d = 0;			// Only used if mflag is unset (mflag == false)

	for (unsigned int iter = 1; iter < max_iter; ++iter)
	{
		// stop if converged on root or error is less than tolerance
		if (abs(b-a) < tol) return s;

		if (fa != fc && fb != fc)
		{
			// use inverse quadratic interopolation
			s =	  ( a * fb * fc / ((fa - fb) * (fa - fc)) )
				+ ( b * fa * fc / ((fb - fa) * (fb - fc)) )
				+ ( c * fa * fb / ((fc - fa) * (fc - fb)) );
		}
		else
		{
			// secant method
			s = b - fb * (b - a) / (fb - fa);
		}

			// checks to see whether we can use the faster converging quadratic && secant methods or if we need to use bisection
		if (	( (s < (3 * a + b) * 0.25) || (s > b) ) ||
				( mflag && (std::abs(s-b) >= (std::abs(b-c) * 0.5)) ) ||
				( !mflag && (std::abs(s-b) >= (std::abs(c-d) * 0.5)) ) ||
				( mflag && (std::abs(b-c) < tol) ) ||
				( !mflag && (std::abs(c-d) < tol))	)
		{
			// bisection method
			s = (a+b)*0.5;

			mflag = true;
		}
		else
		{
			mflag = false;
		}

		fs = f(s);	// calculate fs
		d = c;		// first time d is being used (wasnt used on first iteration because mflag was set)
		c = b;		// set c equal to upper bound
		fc = fb;	// set f(c) = f(b)

		if ( fa * fs < 0)	// fa and fs have opposite signs
		{
			b = s;
			fb = fs;	// set f(b) = f(s)
		}
		else
		{
			a = s;
			fa = fs;	// set f(a) = f(s)
		}

		if (abs(fa) < abs(fb)) // if magnitude of fa is less than magnitude of fb
		{
			std::swap(a,b);		// swap a and b
			std::swap(fa,fb);	// make sure f(a) and f(b) are correct after swap
		}

	} // end for

  return NAN;
} // end brents_fun

}

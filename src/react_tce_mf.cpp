/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "math.h"
#include "math_const.h"
#include "string.h"
#include "stdlib.h"
#include "react_tce_mf.h"
#include "input.h"
#include "update.h"
#include "collide.h"
#include "particle.h"
#include "comm.h"
#include "random_park.h"
#include "memory.h"
#include "error.h"
#include "pointers.h"
#include "math_extra.h"
#include "json.hpp"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <math.h>
#include <mpi.h>
#include <string>

using namespace SPARTA_NS;
using namespace MathConst;
using json = nlohmann::json; // from "json.hpp"

enum{DISSOCIATION,EXCHANGE,IONIZATION,RECOMBINATION};   // other files

#define MAXLINE 1024
#define react_vss_pairparams2D(i,j) ((i <= j)?react_vss_pairparams[i*nparams + j]:react_vss_pairparams[j*nparams + i])


/* ---------------------------------------------------------------------- */

ReactTCEMF::ReactTCEMF(SPARTA *sparta, int narg, char **arg)
    : ReactBird(sparta, narg, arg) {
  if (narg < 3)
    error->all(FLERR, "Illegal react tce/mf command");

  splist_mf = NULL;
  rlist_mf = NULL;
  if (nparams == 0)
    error->all(FLERR,"Cannot use tce/mf command with no species defined");
  react_vss_pairparams = new CollideVSS::PairParams[nparams*nparams];
  readfile_mf(arg[2]);
  if (narg == 4) {
    if (comm->me==0) nreact_vss_pairparams = readfile_mfvss(arg[3]);
    MPI_Bcast(react_vss_pairparams,nparams*nparams*sizeof(CollideVSS::PairParams),MPI_BYTE,0,world);
    MPI_Bcast(&nreact_vss_pairparams, 1, MPI_INT, 0,world);
  }
}

/* ---------------------------------------------------------------------- */

ReactTCEMF::~ReactTCEMF()
{
  if (nsp_mf > 0) {
    for (int i = 0; i < nsp_mf; i++) {
      for (int j = 0; j < splist_mf[i].npartner; j++) {
        delete [] splist_mf[i].name_partner[j];
      }
      delete [] splist_mf[i].name_partner;
      delete [] splist_mf[i].id_partner;
    }
    delete [] splist_mf;
  }
}

/* ---------------------------------------------------------------------- */

void ReactTCEMF::init()
{
  if (!collide || strcmp(collide->style,"vss") != 0)
    error->all(FLERR,"React tce/mf can only be used with collide vss");

  ReactBird::init();

  // Consistent recombination model is not implemented in SPARTA for MF model yet
  // Han Luo has implemented the model in DS1V. Check AIAA Scitech 2020


  for (int i = 0; i < nlist; i++)
    if (rlist[i].active && rlist[i].type == RECOMBINATION)
      error->all(FLERR,
                 "React tce/mf does not currently support recombination reactions");

  // MPI_Barrier(world);
  // std::cout << "Rank " << comm->me << " : " << splist_mf[0].id_partner[1];
  // MPI_Barrier(world);
  // error->all(FLERR, "test");

  rlist_mf = (OneReactionMF *)
    memory->srealloc(rlist_mf, nlist*sizeof(OneReactionMF), "react/mf:rlistmf");

  for (int i = 0; i < nlist; i++) {
    OneReactionMF *rmf = &rlist_mf[i];
    rmf->nangle = 0;       // exchange or other type of reactions
    rmf->double_diss = 0;
    rmf->ispmf = NULL;
    rmf->jspmf = NULL;
    rmf->sec = NULL;
  }
  int ilast_diss = -1, ifirst_other = -1;
  for (int i = 0; i < nlist; i++) {
    OneReaction *r = &rlist[i];
    OneReactionMF *rmf = &rlist_mf[i];

    if (r->type != DISSOCIATION) {
      if (ifirst_other < 0) ifirst_other = i;
      rmf->nangle = -1;
      continue;
    }

    ilast_diss = i;

    int il = r->reactants[0];
    int ir = r->reactants[1];

    OneSpecieMF test;
    test.id = il;
    OneSpecieMF *sp_mf = std::find(splist_mf, splist_mf+nsp_mf, test);
    // no correponding dissociated molecule found
    if (sp_mf == splist_mf + nsp_mf) continue;
    int *imfp = std::find(sp_mf->id_partner, sp_mf->id_partner + sp_mf->npartner, ir);
    // no partner found
    if (imfp == sp_mf->id_partner + sp_mf->npartner) continue;

    int ilvdof = particle->species[il].vibdof;
    if (ilvdof != 2) error->all(FLERR, "Wrong number of vibdof for MF model");
    rmf->ispmf = sp_mf;

    int irvdof = particle->species[ir].vibdof;
    if (irvdof == 0) {
      rmf->jspmf = NULL;
      rmf->nangle = MF_AD_NANGLE;
    } else if (irvdof == 2) {
      test.id = ir;
      OneSpecieMF *sp_mf2 = std::find(splist_mf, splist_mf + nsp_mf, test);
      if (sp_mf2 == splist_mf + nsp_mf)
        error->all(FLERR,
                   "Diatom-diatom dissociation information is not complete");
      rmf->nangle = MF_DD_NANGLE;
      rmf->jspmf = sp_mf2;
      int *imfp2 = std::find(sp_mf2->id_partner, sp_mf2->id_partner + sp_mf2->npartner, il);
      if (imfp2 == sp_mf2->id_partner + sp_mf2->npartner) {
        rmf->double_diss = 0;
      } else {
        rmf->double_diss = 1;
        // Note! the code will next check if the other dissociation channel is also
        // defined in rlist
      }
    } else error->all(FLERR, "Wrong number of vibdof for MF model");
  }

  if (ifirst_other >=0 && ifirst_other < ilast_diss)
    error->all(FLERR, "tce/mf requires you to place dissociation reactions ahead of other type");

  if (nlist > 1) {
    for (int i = 0; i < nlist; i++) {
      OneReactionMF *rmf = &rlist_mf[i];
      if (rmf->double_diss != 1) continue;
      if (rmf->ispmf == rmf->jspmf) {
        rmf->sec = rmf;
        continue;
      }
      OneReactionMF *rmfp = NULL;
      for (int j = 0; j < nlist; j++) {
        if (i == j) continue;
        if (rlist_mf[j] == *rmf) {
          rmfp = &rlist_mf[j];
          rmf->sec = rmfp;
          break;
        }
      }
      if (!rmfp) rmf->double_diss = 0;
    }
  }

  for (int i = 0; i < nlist; i++) {
    OneReactionMF *rmf = &rlist_mf[i];
    if (rmf->nangle <= 0) continue;
    OneReaction *r = &rlist[i];

    rmf->ispa = r->products[0];
    rmf->ispb = r->products[1];

    if (rmf->nangle == MF_AD_NANGLE) {
      rmf->ispc = r->products[2];
      rmf->ispd = -1;
    } else if (rmf->nangle == MF_DD_NANGLE) {
      if (rmf->sec) {
        // obtain from the other reaction channel
        OneReaction *rp = rlist + (rmf->sec - rlist_mf);
        rmf->ispc = rp->products[0];
        rmf->ispd = rp->products[1];
      } else {
        rmf->ispc = -2;
        rmf->ispd = -2;
      }
    }
  }

  // try to figure out ispc = -2 and ispd = -2
  for (int i = 0; i < nlist; i++) {
    OneReactionMF *rmf = &rlist_mf[i];
    OneReaction *r = &rlist[i];
    if (rmf->nangle <= 0) continue;
    if (rmf->ispc == -2) {
      for (int j = 0; j < nlist; j++) {
        OneReactionMF *rmf2 = &rlist_mf[j];
        if (rmf2->nangle <= 0) continue;
        OneReaction *r2 = &rlist[j];
        if (r->reactants[1] == r2->reactants[0]) {
          rmf->ispc = rmf2->ispa;
          rmf->ispd = rmf2->ispb;
          break;
        }
      }
      if (rmf->ispc == -2) {
        error->all(FLERR, "Fail to figure out mc md in tce/mf");
      }
    }
    double ma = particle->species[rmf->ispa].mass;
    double mb = particle->species[rmf->ispb].mass;
    double mc = particle->species[rmf->ispc].mass;
    double md;
    double muab = ma*mb/(ma+mb);
    double mu;
    double mucd;
    if (rmf->nangle == MF_DD_NANGLE) {
      md = particle->species[rmf->ispd].mass;
      mu = (ma + mb) * (mc + md) / (ma + mb + mc + md);
      mucd = mc*md/(mc+md);
      rmf->factor[5] = 2*mucd/mc;
      rmf->factor[6] = 2*sqrt(muab*mucd)/mc;
      rmf->factor[7] = 2*sqrt(muab*mucd)/mb;
    } else {
      mu = (ma+mb)*mc/(ma+mb+mc);
    }
    rmf->factor[0] = mu/muab/4;
    rmf->factor[1] = mu/mucd/4;
    rmf->factor[2] = 1 + mb/mc;
    rmf->factor[3] = 1 + mc/mb;
    rmf->factor[4] = 2*muab/mb;

  }
}

/* ---------------------------------------------------------------------- */

int ReactTCEMF::attempt(Particle::OnePart *ip, Particle::OnePart *jp,
                        double pre_etrans, double pre_erot, double pre_evib,
                        double &post_etotal, int &kspecies)
{
  double pre_etotal;
  double e_excess;
  double ecc;
  int reaction;

  int isp = ip->ispecies;
  int jsp = jp->ispecies;

  called = 0;
  int n = reactions[isp][jsp].n;

  if (n == 0) return 0;
  int *list = reactions[isp][jsp].list;

  // loop over possible reactions for these 2 species
  pre_etotal = pre_etrans + pre_erot + pre_evib;
  ecc = pre_etotal;
  double ethreshold = ecc*2.0, ethreshold1=ecc*2.0,ethreshold2=ecc*2.0;
  int ir1,ir2;
  for (int i = 0; i < n; i++) {
    OneReaction *r = &rlist[list[i]];
    OneReactionMF *rmf = &rlist_mf[list[i]];
    if (rmf->nangle <= 0) {
      // ignore all none MF reactions
      continue;
    }

    // ignore energetically impossible reactions
    e_excess = ecc - rmf->ispmf->D0;
    if (e_excess <= 0.0) continue;

    // Reactants order matters for MF model because we need to know evib for each of them
    if (r->reactants[0] == jsp && r->reactants[1] == isp) {
      std::swap(ip, jp);
      std::swap(isp,jsp);
    }

    if (r->reactants[0] == r->reactants[1]) {
      // O2+O2 => O+O+O2 type dissociation, pick the one with higher vibrational energy
      ethreshold =
          (ip->evib > jp->evib)
              ? evaluate_threshold(i, ip->evib, ip->erot, jp->evib, jp->erot)
              : evaluate_threshold(i, jp->evib, jp->erot, ip->evib, ip->erot);
      ir1 = i;
      break;
    } else if (rmf->nangle == MF_AD_NANGLE) {
      // O2+O type dissociation
      ethreshold = evaluate_threshold(i, ip->evib, ip->erot);
      ir1 = i;
      break;
    } else if (rmf->double_diss == 0) {
      // O2+N2 => O + O + N2, no N2 dissociation
      ethreshold = evaluate_threshold(i, ip->evib, ip->erot, jp->evib, jp->erot);
      ir1 = i;
      break;
    } else if (rmf->double_diss == 1) {
      // two channel dissociation
      if (!called) {
        ethreshold1 = evaluate_threshold(i, ip->evib, ip->erot, jp->evib, jp->erot);
        ir1 = i;
        called = 1;
      } else {
        ethreshold2 = evaluate_threshold(i, ip->evib, ip->erot, jp->evib, jp->erot);
        ir2 = i;
        break;
      }
    }
  }

  int ireac = -1;
  if (called) {
    if (ethreshold2 >= ethreshold1 && pre_etrans > ethreshold1) {
      ireac = ir1;
    }
    if (ethreshold2 <= ethreshold1 && pre_etrans > ethreshold2) {
      ireac = ir2;
    }
  } else {
    if (pre_etrans > ethreshold) {
      ireac = ir1;
    }
  }


  reaction = 0;
  if (ireac >= 0) {
    OneReaction *r = &rlist[list[ireac]];
    OneReactionMF *rmf = &rlist_mf[list[ireac]];
    tally_reactions[list[ireac]]++;

    if (!frozenflag) {
      ip->ispecies = r->products[0];
      jp->ispecies = r->products[1];

      post_etotal = pre_etotal - rmf->ispmf->D0;
      kspecies = r->products[2];
    } else {
      post_etotal = pre_etotal;
      kspecies = -1;
    }
    reaction =  1;
  }

  return reaction;
}

/* ---------------------------------------------------------------------- */

// int ReactTCEMF::attempt_tce(Particle::OnePart *ip, Particle::OnePart *jp,
//                             OneReaction *r,
//                             double pre_etrans, double pre_erot, double pre_evib,
//                             double &post_etotal, int &kspecies)
// {
//   Particle::Species *species = particle->species;
//   int isp = ip->ispecies;
//   int jsp = jp->ispecies;

//   double pre_ave_rotdof = (species[isp].rotdof + species[jsp].rotdof)/2.;

//   // probablity to compare to reaction probability

//   double react_prob = 0.0;
//   double random_prob = random->uniform();

//   double pre_etotal = pre_etrans + pre_erot + pre_evib;

//   double ecc = pre_etrans;
//   if (pre_ave_rotdof > 0.1) ecc += pre_erot*r->coeff[0]/pre_ave_rotdof;

//   double e_excess = ecc - r->coeff[1];
//   if (e_excess > 0.0) return 0;

//   // compute probability of reaction

//   switch (r->type) {
//   case DISSOCIATION:
//   case EXCHANGE:
//     {
//       react_prob += r->coeff[2] *
//         pow(ecc-r->coeff[1],r->coeff[3]) *
//         pow(1.0-r->coeff[1]/ecc,r->coeff[5]);
//       break;
//     }

//   default:
//     error->one(FLERR,"Unknown outcome in reaction");
//     break;
//   }

//   // test against random number to see if this reaction occurs

//   if (react_prob > random_prob) {
//     ip->ispecies = r->products[0];
//     jp->ispecies = r->products[1];

//     post_etotal = pre_etotal + r->coeff[4];
//     if (r->nproduct > 2) kspecies = r->products[2];
//     else kspecies = -1;

//     return 1;
//   }

//   return 0;
// }

/* ---------------------------------------------------------------------- */

// int ReactTCEMF::attempt_mf(Particle::OnePart *ip, Particle::OnePart *jp,
//                            OneReaction * r, OneReactionMF * r2,
//                            double pre_etrans, double pre_erot, double pre_evib,
//                            double &post_etotal, int &kspecies)
// {
//   double prob,evib,inverse_kT;
//   int iv,ilevel,maxlev,limlev;
//   int mspec,aspec;

//   Particle::Species *species = particle->species;
//   int isp = ip->ispecies;
//   int jsp = jp->ispecies;

//   double pre_ave_rotdof = (species[isp].rotdof + species[jsp].rotdof)/2.0;

//   double iomega = collide->extract(isp,"omega");
//   double jomega = collide->extract(jsp,"omega");
//   double omega = 0.5 * (iomega+jomega);

//   // probablity to compare to reaction probability

//   double react_prob = 0.0;
//   double random_prob = random->uniform();

//   double pre_etotal = pre_etrans + pre_erot + pre_evib;

//   double ecc = pre_etrans;
//   if (pre_ave_rotdof > 0.1) ecc += pre_erot*r->coeff[0]/pre_ave_rotdof;

//   double e_excess = ecc - r->coeff[1];
//   if (e_excess > 0.0) return 0;

//   // compute probability of reaction

//   inverse_kT = 1.0 / (update->boltz * species[isp].vibtemp[0]);

//   switch (r->type) {
//   case DISSOCIATION:
//     {
//       ecc = pre_etrans + ip->evib;
//       maxlev = static_cast<int> (ecc * inverse_kT);
//       limlev = static_cast<int> (fabs(r->coeff[1]) * inverse_kT);
//       if (maxlev > limlev) react_prob = 1.0;
//       break;
//     }
//   case EXCHANGE:
//     {
//       if (r->coeff[4] < 0.0 && species[isp].rotdof > 0) {

//         // endothermic reaction

//         ecc = pre_etrans + ip->evib;
//         maxlev = static_cast<int> (ecc * inverse_kT);
//         if (ecc > r->coeff[1]) {

//           // PROB is the probability ratio of eqn (5.61)

//           prob = 0.0;
//           do {
//             iv =  static_cast<int> (random->uniform()*(maxlev+0.99999999));
//             evib = static_cast<double> (iv / inverse_kT);
//             if (evib < ecc) react_prob = pow(1.0-evib/ecc,1.5-omega);
//           } while (random->uniform() < react_prob);

//           ilevel = static_cast<int> (fabs(r->coeff[4]) * inverse_kT);
//           if (iv >= ilevel) react_prob = 1.0;
//         }

//        } else if (r->coeff[4] > 0.0 && species[isp].rotdof > 0) {

//         ecc = pre_etrans + ip->evib;

//         // mspec = post-collision species of the particle
//         // aspec = post-collision species of the atom

//         mspec = r->products[0];
//         aspec = r->products[1];

//         if (species[mspec].rotdof < 2.0)  {
//           mspec = r->products[1];
//             aspec = r->products[0];
//         }

//         // potential post-collision energy

//         ecc += r->coeff[4];
//         maxlev = static_cast<int> (ecc * inverse_kT);
//         do {
//           iv = random->uniform()*(maxlev+0.99999999);
//           evib = static_cast<double>
//             (iv * update->boltz*species[mspec].vibtemp[0]);
//           if (evib < ecc) prob = pow(1.0-evib/ecc,1.5 - r->coeff[6]);
//         } while (random->uniform() < prob);

//         ilevel = static_cast<int>
//           (fabs(r->coeff[4]/update->boltz/species[mspec].vibtemp[0]));
//         if (iv >= ilevel) react_prob = 1.0;
//       }

//       break;
//     }

//   default:
//     error->one(FLERR,"Unknown outcome in reaction");
//     break;
//   }

//   // test against random number to see if this reaction occurs
//   // if it does, reset species of I,J and optional K to product species
//   // important NOTE:
//   //   does not matter what order I,J reactants are in compared
//   //     to order the reactants are listed in the reaction file
//   //   for two reasons:
//   //   a) list of N possible reactions above includes all reactions
//   //      that I,J species are in, regardless of order
//   //   b) properties of pre-reaction state, stored in precoln,
//   //      as computed by setup_collision(),
//   //      and used by perform_collision() after reaction has taken place,
//   //      only store combined properties of I,J,
//   //      nothing that is I-specific or J-specific

//   if (react_prob > random_prob) {
//     ip->ispecies = r->products[0];
//     jp->ispecies = r->products[1];

//     post_etotal = pre_etotal + r->coeff[4];
//     if (r->nproduct > 2) kspecies = r->products[2];
//     else kspecies = -1;

//     return 1;
//   }

//   return 0;
// }

/* ---------------------------------------------------------------------
   Read input file for MF model
--------------------------------------------------------------------- */

int ReactTCEMF::readfile_mf(char *fname)
{
  /* The file should provide the properties of different diatomic molecules
    and whether its dissociation reactions are modeled with MF-SHO model
    or MF-AHO model
  */
  if (comm->me == 0) {
    std::ifstream fs(fname);
    if (!fs.is_open()) {
      char str[128];
      sprintf(str,"Cannot open file %s for tce/mf mode",fname);
      error->one(FLERR,str);
    }
    std::string line;
    std::string file_contents;
    while (std::getline(fs, line)) {
      file_contents += line;
      file_contents.push_back('\n');
    }
    fs.close();

    auto jq = json::parse(file_contents);

    // remove species
    //   - not defined by specie
    // remove partner not defined by specie
    if (jq.size() == 0) error->all(FLERR, "Fail to parser tce/mf input file");

    std::vector<std::string> valid_sp_name;
    std::vector<std::vector<std::string>> sp_partners;
    for (json::iterator it = jq.begin(); it != jq.end(); ++it) {
      char sp_name[SPARTA_REACT_TCE_MF_STRLEN];
      strcpy(sp_name, it.key().c_str());
      int isp = particle->find_species(sp_name);
      if (isp < 0) continue;
      if (it.value()["partner"].size() > 0) {
        std::vector<std::string> vjp = it.value()["partner"];
        std::vector<std::string> vp;
        for (int i = 0; i < vjp.size(); i++) {
          char pname[SPARTA_REACT_TCE_MF_STRLEN];
          strcpy(pname, vjp[i].c_str());
          if (particle->find_species(pname) < 0) continue;
          vp.push_back(vjp[i]);
        }
        if (vp.size() > 0) {
          sp_partners.push_back(vp);
          valid_sp_name.push_back(sp_name);
        }
      }
    }
    nsp_mf = valid_sp_name.size();
    splist_mf = (OneSpecieMF *)memory->srealloc(splist_mf,
                  nsp_mf*sizeof(OneSpecieMF), "react_mf::splist_mf");

    // add information back
    for (int i = 0; i < nsp_mf; i++) {
      std::string it = valid_sp_name[i];
      auto j2 = jq[it];

      OneSpecieMF *vsp = &splist_mf[i];
      strcpy(vsp->name, it.c_str());

      vsp->id    = particle->find_species(vsp->name);
      vsp->iaho  = j2["iaho"];
      vsp->De    = j2["De"];
      vsp->De    *= update->ev2e;
      vsp->re    = j2["re"];
      vsp->alpha = j2["alpha"];
      vsp->D0    = j2["D0"];
      vsp->D0    *= update->ev2e;
      vsp->Ev0    = j2["Ev0"];
      vsp->Ev0    *= update->ev2e;
      vsp->npartner = sp_partners[i].size();
      vsp->id_partner = new int[vsp->npartner];
      vsp->name_partner = new char*[vsp->npartner];

      for (int j = 0; j < vsp->npartner; j++) {
        vsp->name_partner[j] = new char[SPARTA_REACT_TCE_MF_STRLEN];
        strcpy(vsp->name_partner[j] , sp_partners[i][j].c_str());
        int partner_id = particle->find_species(vsp->name_partner[j]);
        vsp->id_partner[j] = partner_id;
      }
    }
  }
  MPI_Bcast(&nsp_mf, 1, MPI_INT, 0, world);
  if (comm->me != 0) {
    splist_mf = (OneSpecieMF *)memory->srealloc(splist_mf,
                  nsp_mf*sizeof(OneSpecieMF), "react_mf::splist_mf");
  }
  MPI_Bcast(splist_mf, nsp_mf*sizeof(OneSpecieMF), MPI_BYTE, 0, world);
  if (comm->me != 0) {
    for (int i = 0; i < nsp_mf; i++) {
      OneSpecieMF *vsp = &splist_mf[i];
      vsp->id_partner = new int[vsp->npartner];
      vsp->name_partner = new char*[vsp->npartner];
      for (int j = 0; j < vsp->npartner; j++) {
        vsp->name_partner[j] = new char[SPARTA_REACT_TCE_MF_STRLEN];
      }
    }
  }
  for (int i = 0; i < nsp_mf; i++) {
    MPI_Bcast(splist_mf[i].id_partner, splist_mf[i].npartner, MPI_INT, 0,
              world);
  }
  for (int i = 0; i < nsp_mf; i++) {
    OneSpecieMF *vsp = &splist_mf[i];
    for (int j = 0; j < vsp->npartner; j++) {
      MPI_Bcast(vsp->name_partner[j], SPARTA_REACT_TCE_MF_STRLEN, MPI_CHAR, 0,
                world);
    }
  }

  return nsp_mf;
}

/* ---------------------------------------------------------------------
   Read input file for MF model
--------------------------------------------------------------------- */

int ReactTCEMF::readfile_mfvss(char *fname)
{
  FILE *fp = fopen(fname,"r");
  if (fp == NULL) {
    char str[128];
    sprintf(str,"Cannot open MF VSS parameter file %s",fname);
    error->one(FLERR,str);
  }

  // set all diameters to -1, so can detect if not read
  for (int i = 0; i < nparams*nparams; i++) react_vss_pairparams[i].diam = -1.0;

  // read file line by line
  // skip blank lines or comment lines starting with '#'
  // all other lines must have at least NWORDS

  int NWORDS = 6;
  char **words = new char*[NWORDS];
  char line[MAXLINE],copy[MAXLINE];
  int isp;
  int m = 0;
  while (fgets(line,MAXLINE,fp)) {
    int pre = strspn(line," \t\n\r");
    if (pre == strlen(line) || line[pre] == '#') continue;

    strcpy(copy,line);
    int nwords = CollideVSS::wordcount(copy);
    if (nwords == 6) {
      // VHS pair data
      CollideVSS::wordparse(NWORDS,line,words);
      isp = particle->find_species(words[0]);
      int jsp = particle->find_species(words[1]);
      if (isp < 0 || jsp < 0) continue;
      CollideVSS::PairParams *pp = &react_vss_pairparams2D(isp, jsp);
      pp->diam = atof(words[2]);
      pp->omega = atof(words[3]);
      pp->tref = atof(words[4]);
      pp->alpha = atof(words[5]);
      m++;
    } else {
      error->one(FLERR,"Incorrect line format in VSS parameter file");
    }
  }

  delete [] words;
  fclose(fp);
  return m;
}

/* --------------------------------------------------------------------------
    Sample a phase angle for vibration of a specie
    Arguments:
      - mfsp: pointer to the data of the specie
      - evib: vibrational energy, this one should contain zero point energy
      - r: a random number between 0 and 1
---------------------------------------------------------------------------- */

double ReactTCEMF::sample_vibration_phase(OneSpecieMF *mfsp, double evib) {
  if (mfsp->iaho == 0)
    return random->uniform() * MY_PI;

  double r = random->uniform();
  double rho = 1 + (evib - mfsp->D0) / mfsp->De;  // get energy above Morse potential valley
  if (rho < 0.0)
    rho = 0.0;
  else
    rho = sqrt(rho);
  double theta0 = asin(rho);
  double r2 = cos(theta0) * cos(2 * MY_PI * r - theta0) /
              (1 + rho * sin(2 * MY_PI * r - theta0));
  if (r2 < -1.0)
    r2 = -1.0;
  if (r2 > 1.0)
    r2 = 1.0;
  if (r < theta0 / 2.0 / MY_PI + 0.5)
    return acos(r2);
  else
    return 2.0 * MY_PI - acos(r2);
}

/* -------------------------------------------------------------------------
   Sample phase angle for a colliding pair with given vibrational energy
   ----------------------------------------------------------------------- */
// Note evib1 and evib2 should have zero point energy
// atom-diaom collision, single reaction channel
void ReactTCEMF::sample_phase(int ilist, double evib1) {
  // AB + C
  fgamma_1 = MY_PI * random->uniform();
  fgamma_2 = MY_PI * random->uniform();
  ftheta = MY_PI * random->uniform();
  fphi_0 = sample_vibration_phase(rlist_mf[ilist].ispmf, evib1);
}

void ReactTCEMF::sample_phase(int ilist, double evib1, double evib2) {
  fgamma_1 = MY_PI * random->uniform();
  fgamma_2 = MY_PI * random->uniform();
  ftheta = 2.0 * MY_PI * random->uniform();
  fphi_0 = sample_vibration_phase(rlist_mf[ilist].ispmf, evib1);
  fphi_1 = sample_vibration_phase(rlist_mf[ilist].jspmf, evib2);
  fbeta_1 = MY_PI * random->uniform() - MY_PI * 0.5; // beta [-pi/2, pi/2]
  fbeta_2 = 2.0 * MY_PI * random->uniform();
  fdelta = 2.0 * MY_PI * random->uniform();
  fbeta = MY_PI * (random->uniform() - 0.5);
}

/* -------------------------------------------------------------------------
   Function to evaluate threshold energy
   Arguments:
    - ev1: vibrational energy in joule
    - er1: rotational energy in joule

  To fix:
    the current implementation may be not physical if the molecule is not
    homonuclear molecule, for example NO + N2 dissociation
    should we use N and b atom or O as b atom
   ----------------------------------------------------------------------- */
double  ReactTCEMF::evaluate_threshold(int ilist, double ev1, double er1) {
  OneReactionMF *rmf = &rlist_mf[ilist];
  double diss = rmf->ispmf->D0;           // dissociation energy
  double dstar = diss - er1 + 2.0/3.0*pow(er1,1.5)/ sqrt(6.0 * diss);
  ev1 += rmf->ispmf->Ev0;  // add zero point energy

  sample_phase(ilist, ev1);
  double aa = dstar - ev1 * pow(sin(fphi_0), 2.0);
  if (aa <= 0) return 0.0;
  double b1 = (sqrt(aa) + sqrt(ev1)*cos(fphi_0)) / cos(ftheta) * rmf->factor[2];
  double b2 = -rmf->factor[4] * sqrt(ev1) * cos(fphi_0) * cos(ftheta);
  return rmf->factor[0]/pow(cos(fgamma_2)*cos(fgamma_1),2.0)*pow(b1+b2,2.0);
}

double ReactTCEMF::evaluate_threshold(int ilist, double ev1, double er1,
                                      double ev2, double er2) {
  OneReactionMF *rmf = &rlist_mf[ilist];
  double diss = rmf->ispmf->D0;           // dissociation energy
  double dstar = diss - er1 + 2.0/3.0*pow(er1,1.5)/ sqrt(6.0* diss);

  ev1 += rmf->ispmf->Ev0;  // add zero point energy
  ev2 += rmf->jspmf->Ev0;  // add zero point energy

  if (!called) sample_phase(ilist, ev1, ev2);

  if (!called) {
    double aa = dstar - ev1 * pow(sin(fphi_0), 2.0);
    if (aa <= 0) return 0.0;
    double b1 = (sqrt(aa) + sqrt(ev1)*cos(fphi_0)) / cos(ftheta) * rmf->factor[2];
    double b2 = -rmf->factor[4] * sqrt(ev1) * cos(fphi_0) * cos(ftheta);
    double c1 = sqrt(er2)*(cos(fdelta)*cos(fbeta_2)*sin(fbeta_1)+sin(fdelta)*sin(fbeta_2));
    double c2 = -sqrt(ev2)*cos(fphi_1)*cos(fbeta_1)*cos(fbeta_2);
    double b3 = -rmf->factor[6] * (c1+c2);
    return rmf->factor[0]/pow(cos(fgamma_2)*cos(fgamma_1),2.0)*pow(b1+b2+b3,2.0);
  } else {
    rmf = rmf->sec;  // use the paramter calculated for the other channel
    double aa = dstar - ev1 * pow(sin(fphi_1), 2.0);
    if (aa <= 0) return 0.0;
    double b1 = (sqrt(aa) + sqrt(ev1)*cos(fphi_1)) / cos(fbeta_1) / cos(fbeta_2) * rmf->factor[3];
    double b2 = -rmf->factor[5] * sqrt(ev1) * cos(fphi_1) * cos(fbeta_1) * cos(fbeta_2) ;
    double c1 = sqrt(er2)*cos(fbeta)*sin(ftheta);
    double c2 = -sqrt(ev2)*cos(fphi_0)*cos(ftheta);
    double b3 = -rmf->factor[7] * (c1+c2);
    return rmf->factor[1]/pow(cos(fgamma_2)*cos(fgamma_1),2.0)*pow(b1+b2+b3,2.0);
  }
}

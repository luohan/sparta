/* ----------------------------------------------------------------------
   SPARTA - Stochastic PArallel Rarefied-gas Time-accurate Analyzer
   http://sparta.sandia.gov
   Steve Plimpton, sjplimp@sandia.gov, Michael Gallis, magalli@sandia.gov
   Sandia National Laboratories

   Copyright (2014) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level SPARTA directory.
------------------------------------------------------------------------- */

#include "math.h"
#include "stdlib.h"
#include "string.h"
#include "compute_trot_grid.h"
#include "particle.h"
#include "mixture.h"
#include "grid.h"
#include "update.h"
#include "modify.h"
#include "comm.h"
#include "memory.h"
#include "error.h"

using namespace SPARTA_NS;

// user keywords

enum{TEMP,PRESS};

/* ---------------------------------------------------------------------- */

ComputeTrotGrid::ComputeTrotGrid(SPARTA *sparta, int narg, char **arg) :
  Compute(sparta, narg, arg)
{
  if (narg != 4) error->all(FLERR,"Illegal compute thermal/grid command");

  int igroup = grid->find_group(arg[2]);
  if (igroup < 0) error->all(FLERR,"Compute grid group ID does not exist");
  groupbit = grid->bitmask[igroup];

  imix = particle->find_mixture(arg[3]);
  if (imix < 0)
    error->all(FLERR,"Compute thermal/grid mixture ID does not exist");
  ngroup = particle->mixture[imix]->ngroup;

  per_grid_flag = 1;
  size_per_grid_cols = ngroup;
  post_process_grid_flag = 1;

  // allocate and initialize nmap and map
  // npergroup = 2 tally quantities per group
  // same tally quantities for all user values

  npergroup = 2;
  ntotal = ngroup*npergroup;
  nmap = npergroup;

  memory->create(map,ngroup,npergroup,"thermal/grid:map");
  for (int i = 0; i < ngroup; i++)
    for (int j = 0; j < npergroup; j++)
      map[i][j] = i*npergroup + j;

  nglocal = 0;
  vector_grid = NULL;
  tally = NULL;
}

/* ---------------------------------------------------------------------- */

ComputeTrotGrid::~ComputeTrotGrid()
{
  if (copymode) return;

  memory->destroy(map);

  memory->destroy(vector_grid);
  memory->destroy(tally);
}

/* ---------------------------------------------------------------------- */

void ComputeTrotGrid::init()
{
  if (ngroup != particle->mixture[imix]->ngroup)
    error->all(FLERR,"Number of groups in compute thermal/grid "
               "mixture has changed");

  prefactor = update->mvv2e / update->boltz;

  reallocate();
}

/* ---------------------------------------------------------------------- */

void ComputeTrotGrid::compute_per_grid()
{
  invoked_per_grid = update->ntimestep;

  Grid::ChildInfo *cinfo = grid->cinfo;
  Particle::Species *species = particle->species;
  Particle::OnePart *particles = particle->particles;
  int *s2g = particle->mixture[imix]->species2group;
  int nlocal = particle->nlocal;

  // zero all accumulators - could do this with memset()

  for (int i = 0; i < nglocal; i++)
    for (int j = 0; j < ntotal; j++)
      tally[i][j] = 0.0;

  // loop over all particles, skip species not in mixture group

  for (int i = 0; i < nlocal; i++) {
    int ispecies = particles[i].ispecies;
    int igroup = s2g[ispecies];
    if (igroup < 0) continue;
    int icell = particles[i].icell;
    if (!(cinfo[icell].mask & groupbit)) continue;

    // 6 tallies per particle: N, EROT

    double *vec = tally[icell];
    int k = igroup*npergroup;

    vec[k++] += species[ispecies].rotdof / 2.0;
    vec[k++] += particles[i].erot;
  }
}

/* ----------------------------------------------------------------------
   query info about internal tally array for this compute
   index = which column of output (0 for vec, 1 to N for array)
   return # of tally quantities for this index
   also return array = ptr to tally array
   also return cols = ptr to list of columns in tally for this index
------------------------------------------------------------------------- */

int ComputeTrotGrid::query_tally_grid(int index, double **&array, int *&cols)
{
  index--;
  array = tally;
  cols = map[index];
  return nmap;
}

/* ----------------------------------------------------------------------
   tally accumulated info to compute final normalized values
   index = which column of output (0 for vec, 1 to N for array)
   for etally = NULL:
     use internal tallied info for single timestep, set nsample = 1
     compute values for all grid cells
       store results in vector_grid with nstride = 1 (single col of array_grid)
   for etally = ptr to caller array:
     use external tallied info for many timesteps
     nsample = additional normalization factor used by some values
     emap = list of etally columns to use, # of columns determined by index
     store results in caller's vec, spaced by nstride
   if norm = 0.0, set result to 0.0 directly so do not divide by 0.0
------------------------------------------------------------------------- */

void ComputeTrotGrid::
post_process_grid(int index, int nsample,
                  double **etally, int *emap, double *vec, int nstride)
{
  index--;
  if (!etally) {
    nsample = 1;
    etally = tally;
    emap = map[index];
    vec = vector_grid;
    nstride = 1;
  }

  int n = emap[0];
  int k = 0;
  int lo = 0;
  int hi = nglocal;

  for (int icell = lo; icell < hi; icell++) {
    double *values = etally[icell];
    double denom = values[n];
    if (denom == 0.0) vec[k] = 0.0;
    else vec[k] = values[n+1] / denom * prefactor;
    k += nstride;
  }
}

/* ----------------------------------------------------------------------
   reallocate arrays if nglocal has changed
   called by init() and whenever grid changes
------------------------------------------------------------------------- */

void ComputeTrotGrid::reallocate()
{
  if (grid->nlocal == nglocal) return;

  memory->destroy(vector_grid);
  memory->destroy(tally);
  nglocal = grid->nlocal;
  memory->create(vector_grid,nglocal,"trot/grid:vector_grid");
  memory->create(tally,nglocal,ntotal,"trot/grid:tally");
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based data
------------------------------------------------------------------------- */

bigint ComputeTrotGrid::memory_usage()
{
  bigint bytes = 0;
  bytes = nglocal * sizeof(double);
  bytes = ntotal*nglocal * sizeof(double);
  return bytes;
}

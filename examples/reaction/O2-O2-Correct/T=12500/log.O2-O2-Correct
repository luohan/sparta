SPARTA (7 May 2020)
# test of vibrational energy modes
# thermal gas in a 3d box with collisions
# particles reflect off global box boundaries
# Tips:
# - To create particles with vibrational energy, `collide_modify vibrate discrete` needs to be ahead of create_particles
#
variable            Teq equal 12500.0
variable            mfp0  equal 0.00178   #mfp for 1000K,1e21
variable            mct   equal 2.169e-6
variable            n0    equal 1E21
variable            boxsize equal 0.01
variable            GNX0  equal ceil(${boxsize}/${mfp0})*2 # every grid is half mfp size
variable            GNX0  equal ceil(0.01/${mfp0})*2 
variable            GNX0  equal ceil(0.01/0.00178)*2 
variable            GNY0  equal ceil(${boxsize}/${mfp0})*2
variable            GNY0  equal ceil(0.01/${mfp0})*2
variable            GNY0  equal ceil(0.01/0.00178)*2
variable            FNUM  equal ${n0}*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) #every cell has 1e4 molecules
variable            FNUM  equal 1e+21*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*12*1e4) 
variable            zoomfactor equal 1/${boxsize}
variable            zoomfactor equal 1/0.01
variable            NE equal 100
variable            ND equal 100
variable            NR equal 500
variable            NMOL equal ${n0}*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/69444444444.4444
print               "NMOL=${n0}*${boxsize}*${boxsize}/${FNUM}"
NMOL=1e+21*0.01*0.01/69444444444.4444
print               "FNUM=${FNUM}"
FNUM=69444444444.4444
print               "GNX0=${GNX0} GNY0=${GNY0}"
GNX0=12 GNY0=12
print               "NMOL=${NMOL}"
NMOL=1440000
timestep            1e-7
seed	    	    1235
global              nrho ${n0} fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum 69444444444.4444 comm/sort yes
# grid info
boundary            r r p
create_box          0 ${boxsize} 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 0.01 -0.5 0.5
Created orthogonal box = (0 0 -0.5) to (0.01 0.01 0.5)
create_grid         ${GNX0} ${GNY0} 1
create_grid         12 ${GNY0} 1
create_grid         12 12 1
Created 144 child grid cells
  parent cells = 1
  CPU time = 0.000646151 secs
  create/ghost percent = 87.0888 12.9112

# surf info

# write grid
balance_grid        rcb cell
Balance grid migrated 72 cells
  CPU time = 0.000151655 secs
  reassign/sort/migrate/ghost percent = 54.4163 0.482015 21.9993 23.1024
fix                 gridcheck grid/check 1 warn
write_grid          parent Grid0_partent.grid
  parent cells = 1
  CPU time = 6.5312e-05 secs
write_grid          geom   Grid0_geom.grid
  parent cells = 1
  CPU time = 0.000957114 secs

# particle info
species		    air.species O2 O vlevelfile air_vlevel.json
mixture             background O2 frac 1.0 temp ${Teq} trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 12500 trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 12500 trot 12500 tvib ${Teq}
mixture             background O2 frac 1.0 temp 12500 trot 12500 tvib 12500
mixture             background O frac 0.0
collide             vss background air.vss relax variable isothermal no relaxtemp temp
collide_modify      rotate smooth vibrate discrete
react               tce air.tce
create_particles    background n 0
Created 1440000 particles
  CPU time = 0.626638 secs

# compute
compute             1    grid         all all n nrho massrho u v
compute             2    thermal/grid all all temp press
compute             3    tvib/grid    all species
compute             4    trot/grid    all all
compute             5    grid         all species n
variable            ftime equal dt*step

# fix
fix                 1   ave/grid     all 1 ${NE} ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 100 c_2[1] c_4[1] c_3[*] ave one
#fix                 2   ave/grid     all 1 ${NE} ${NE} c_1[*] ave one
fix                 3   ave/grid     all 1 ${NE} ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 100 c_5[*] ave one
compute             6   reduce ave   f_1[*]
compute             7   reduce sum   c_5[*]

# modify dependent on fix
# 1. use cell temperature for relaxation, this must be placed behind fix ave/grid and collide_modify of vibrate
collide_modify      temp f_1[*]
# 2. include vibrational dof in TCE, this must be placed behind collide_modify
react_modify        vdof yes frozen yes


# dump
# dump                1 grid  all ${ND} flowfield.out.* id xc yc f_1[*]
#dump               3 image all 50 dump.*.ppm type type pdiam 0.0001 view 0.0 0.0 size 1024 1024 zoom ${zoomfactor} gline yes 0.01

# stat
stats_style	    step cpu np nattempt nreact ncoll v_ftime c_6[*] c_7[*]
stats		    ${NE}
stats		    100
# restart
run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 74.25 74.25 74.25
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 75.7848 75.7848 75.7848
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
       0            0  1440000        0        0        0            0            0            0            0            0      1440000            0 
     100    27.105963  1440000   198783     2637    65120        1e-05    12492.938    12508.663    12485.893            0      1440000            0 
     200    54.652726  1440000   198790     2577    65614        2e-05    12486.148    12511.868     12491.87            0      1440000            0 
     300    82.392757  1440000   198791     2571    65193        3e-05    12495.716     12494.46    12494.399            0      1440000            0 
     400    109.59328  1440000   198778     2716    65223        4e-05    12488.252    12500.148    12499.137            0      1440000            0 
     500     137.5682  1440000   198791     2588    65370        5e-05    12485.899     12499.74     12502.48            0      1440000            0 
     600     165.4635  1440000   198780     2646    65284        6e-05    12487.753    12504.797    12495.818            0      1440000            0 
     700    192.97927  1440000   198790     2633    65027        7e-05    12488.397    12508.245    12492.039            0      1440000            0 
     800    220.50377  1440000   198784     2660    65621        8e-05    12498.824    12489.776    12494.475            0      1440000            0 
     900    248.13094  1440000   198791     2620    65435        9e-05    12494.103     12494.26    12496.675            0      1440000            0 
    1000    275.27323  1440000   198786     2609    64991       0.0001    12494.748    12498.942    12491.837            0      1440000            0 
    1100    302.62994  1440000   198793     2661    65411      0.00011    12499.456    12496.376    12488.035            0      1440000            0 
    1200    329.67311  1440000   198774     2621    65385      0.00012    12496.961    12495.135    12492.269            0      1440000            0 
    1300    357.21587  1440000   198790     2587    65052      0.00013    12491.362      12501.5     12494.01            0      1440000            0 
    1400    385.08684  1440000   198788     2598    65547      0.00014    12493.824    12496.505    12495.117            0      1440000            0 
    1500    412.89707  1440000   198784     2619    65244      0.00015    12490.131    12498.586    12498.027            0      1440000            0 
    1600    440.24227  1440000   198778     2696    65309      0.00016    12488.884    12497.448     12500.61            0      1440000            0 
    1700     468.0802  1440000   198782     2678    65389      0.00017    12499.819    12486.821    12495.714            0      1440000            0 
    1800    495.44157  1440000   198784     2567    65088      0.00018    12496.235    12493.437    12494.643            0      1440000            0 
    1900    523.35536  1440000   198791     2640    65194      0.00019    12490.853    12498.872    12496.903            0      1440000            0 
    2000    550.84932  1440000   198787     2608    65349       0.0002     12479.82    12514.933    12497.328            0      1440000            0 
Loop time of 550.849 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.443     | 40.841     | 41.238     |   6.2 |  7.41
Coll    | 304.74     | 306.44     | 308.14     |   9.7 | 55.63
Sort    | 31.756     | 31.759     | 31.762     |   0.1 |  5.77
Comm    | 6.7304     | 6.7896     | 6.8487     |   2.3 |  1.23
Modify  | 162.55     | 164.59     | 166.62     |  15.9 | 29.88
Output  | 0.42694    | 0.43176    | 0.43658    |   0.7 |  0.08
Other   |            | 0.001471   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3791506850 (3.79B)
Particle comms    = 41421126 (41.4M)
Boundary collides = 82826064 (82.8M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 397572736 (398M)
Collide occurs    = 130723856 (131M)
Reactions         = 5282621 (5.28M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.61414e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.3165
Particle comm iterations/step: 1
Particle fraction communicated: 0.0143823
Particle fraction colliding with boundary: 0.0287591
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.138046
Collisions/particle/step: 0.0453902
Reactions/particle/step: 0.00183424

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 5.28262e+06
    number of reactions of real particle: 3.66849e+17
    reaction rate: 1.83424e-17 (m^3/s)
    reaction rate: 1.10461e+13 (cm^3/mol/s)

Particles: 720000 ave 721210 max 718790 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    2000            0  1440000   198787     2608    65349       0.0002     12479.82    12514.933    12497.328            0      1440000            0 
    2100    27.705738  1440000   198780     2622    65329      0.00021    12482.354    12512.617     12496.04            0      1440000            0 
    2200    55.560108  1440000   198787     2594    65390      0.00022    12496.149    12497.013    12491.713            0      1440000            0 
    2300    82.803922  1440000   198788     2667    65706      0.00023    12491.216    12505.877    12490.469            0      1440000            0 
    2400    110.97947  1440000   198787     2631    65063      0.00024    12492.777    12502.475    12491.351            0      1440000            0 
    2500    138.80158  1440000   198790     2583    65381      0.00025    12498.082    12491.273    12494.132            0      1440000            0 
    2600    166.30598  1440000   198794     2769    65490      0.00026    12503.588    12486.726    12490.975            0      1440000            0 
    2700    193.22935  1440000   198790     2698    65404      0.00027    12498.667    12493.303    12491.678            0      1440000            0 
    2800    220.49267  1440000   198788     2675    65190      0.00028    12500.331    12491.767    12490.831            0      1440000            0 
    2900    247.95252  1440000   198785     2595    65286      0.00029    12493.831    12506.672    12486.426            0      1440000            0 
    3000    274.82008  1440000   198786     2656    65707       0.0003    12494.922    12507.716    12484.164            0      1440000            0 
    3100    302.15676  1440000   198783     2708    65523      0.00031    12501.706    12497.071    12484.558            0      1440000            0 
    3200    329.90345  1440000   198792     2652    65477      0.00032    12498.519     12496.78    12488.886            0      1440000            0 
    3300    357.69728  1440000   198787     2638    65347      0.00033    12490.709    12508.201    12489.126            0      1440000            0 
    3400    385.00187  1440000   198780     2685    65440      0.00034    12500.979    12496.883     12485.62            0      1440000            0 
    3500    412.64324  1440000   198790     2672    65343      0.00035    12493.821     12504.27    12488.489            0      1440000            0 
    3600    440.06206  1440000   198786     2647    65373      0.00036    12501.848     12503.86    12478.596            0      1440000            0 
    3700    466.94487  1440000   198789     2634    65234      0.00037    12496.167    12507.608    12482.667            0      1440000            0 
    3800    494.34113  1440000   198779     2681    65400      0.00038    12495.807    12509.016    12481.906            0      1440000            0 
    3900    522.03398  1440000   198780     2739    65566      0.00039    12498.702    12502.255    12483.958            0      1440000            0 
    4000    549.22665  1440000   198787     2671    65303       0.0004    12501.425      12488.7     12492.03            0      1440000            0 
Loop time of 549.227 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 41.325     | 41.725     | 42.125     |   6.2 |  7.60
Coll    | 298.68     | 298.84     | 298.99     |   0.9 | 54.41
Sort    | 31.841     | 31.853     | 31.865     |   0.2 |  5.80
Comm    | 6.8987     | 6.908      | 6.9172     |   0.4 |  1.26
Modify  | 169.28     | 169.51     | 169.74     |   1.8 | 30.86
Output  | 0.3933     | 0.39743    | 0.40155    |   0.7 |  0.07
Other   |            | 0.001542   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3791694261 (3.79B)
Particle comms    = 41427433 (41.4M)
Boundary collides = 82841998 (82.8M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 397572455 (398M)
Collide occurs    = 130728951 (131M)
Reactions         = 5286593 (5.29M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.62187e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.31656
Particle comm iterations/step: 1
Particle fraction communicated: 0.0143845
Particle fraction colliding with boundary: 0.0287646
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.138046
Collisions/particle/step: 0.045392
Reactions/particle/step: 0.00183562

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 5.28659e+06
    number of reactions of real particle: 3.67125e+17
    reaction rate: 1.83562e-17 (m^3/s)
    reaction rate: 1.10544e+13 (cm^3/mol/s)

Particles: 720000 ave 720513 max 719487 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    4000            0  1440000   198787     2671    65303       0.0004    12501.425      12488.7     12492.03            0      1440000            0 
    4100    27.595372  1440000   198786     2620    65318      0.00041    12495.954     12493.86    12494.652            0      1440000            0 
    4200     55.13639  1440000   198794     2627    65054      0.00042    12488.254    12504.302    12495.606            0      1440000            0 
    4300    82.803232  1440000   198790     2646    65543      0.00043    12490.132    12508.776    12489.366            0      1440000            0 
    4400     110.4376  1440000   198784     2570    65463      0.00044    12502.553    12498.797    12481.991            0      1440000            0 
    4500    137.22067  1440000   198778     2646    65383      0.00045    12497.108    12499.573    12488.291            0      1440000            0 
    4600    164.77599  1440000   198787     2576    65337      0.00046    12493.253    12498.056    12494.538            0      1440000            0 
    4700    192.09729  1440000   198784     2705    65174      0.00047    12495.204    12494.885    12494.721            0      1440000            0 
    4800    219.90462  1440000   198784     2672    65363      0.00048    12499.112    12496.116      12488.7            0      1440000            0 
    4900    247.37965  1440000   198782     2730    65699      0.00049    12501.886    12498.481    12483.148            0      1440000            0 
    5000    274.17254  1440000   198787     2666    65355       0.0005    12496.205    12502.411    12487.038            0      1440000            0 
    5100     302.2462  1440000   198780     2694    65174      0.00051    12497.939    12497.039    12489.406            0      1440000            0 
    5200    329.59899  1440000   198786     2598    65165      0.00052    12497.923    12497.104     12489.37            0      1440000            0 
    5300    356.22239  1440000   198794     2567    65306      0.00053    12495.064    12498.085    12492.184            0      1440000            0 
    5400    384.20825  1440000   198784     2660    65411      0.00054    12496.171     12500.53    12488.687            0      1440000            0 
    5500    412.21477  1440000   198786     2691    65477      0.00055    12492.086    12503.459    12491.416            0      1440000            0 
    5600    440.37688  1440000   198795     2633    65199      0.00056    12491.769    12510.096    12486.168            0      1440000            0 
    5700    467.70667  1440000   198798     2631    65153      0.00057    12490.725    12513.797    12484.359            0      1440000            0 
    5800    495.03032  1440000   198788     2590    65217      0.00058    12492.472    12515.507    12480.631            0      1440000            0 
    5900    523.15419  1440000   198793     2649    65348      0.00059    12497.682    12504.919    12483.006            0      1440000            0 
    6000    550.89571  1440000   198784     2506    65407       0.0006    12491.668    12507.076    12488.847            0      1440000            0 
Loop time of 550.896 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 41.712     | 42.129     | 42.546     |   6.4 |  7.65
Coll    | 293.78     | 295.71     | 297.64     |  11.2 | 53.68
Sort    | 33.357     | 33.359     | 33.361     |   0.0 |  6.06
Comm    | 7.0084     | 7.0522     | 7.096      |   1.6 |  1.28
Modify  | 169.73     | 172.12     | 174.52     |  18.3 | 31.24
Output  | 0.51368    | 0.52153    | 0.52939    |   1.1 |  0.09
Other   |            | 0.001545   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3791612328 (3.79B)
Particle comms    = 41416976 (41.4M)
Boundary collides = 82834950 (82.8M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 397572465 (398M)
Collide occurs    = 130722344 (131M)
Reactions         = 5280210 (5.28M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.61392e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.31653
Particle comm iterations/step: 1
Particle fraction communicated: 0.0143809
Particle fraction colliding with boundary: 0.0287621
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.138046
Collisions/particle/step: 0.0453897
Reactions/particle/step: 0.00183341

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 5.28021e+06
    number of reactions of real particle: 3.66681e+17
    reaction rate: 1.83341e-17 (m^3/s)
    reaction rate: 1.1041e+13 (cm^3/mol/s)

Particles: 720000 ave 720421 max 719579 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    6000            0  1440000   198784     2506    65407       0.0006    12491.668    12507.076    12488.847            0      1440000            0 
    6100    27.357837  1440000   198780     2681    65508      0.00061    12496.312    12500.481    12488.549            0      1440000            0 
    6200    55.197246  1440000   198788     2545    65274      0.00062    12493.385    12505.623    12487.903            0      1440000            0 
    6300    83.138242  1440000   198789     2617    65223      0.00063    12496.365    12496.225    12492.115            0      1440000            0 
    6400    110.79166  1440000   198779     2617    65064      0.00064    12504.321    12480.791    12495.115            0      1440000            0 
    6500    138.19035  1440000   198790     2688    65272      0.00065     12497.67    12488.067    12497.409            0      1440000            0 
    6600    165.58488  1440000   198784     2644    65637      0.00066    12494.752        12497    12493.486            0      1440000            0 
    6700    193.08036  1440000   198784     2669    65411      0.00067    12491.564    12505.145    12490.632            0      1440000            0 
    6800    220.82055  1440000   198784     2625    65918      0.00068    12491.403    12501.899    12493.614            0      1440000            0 
    6900    248.54541  1440000   198787     2594    65417      0.00069    12497.211    12497.155    12490.232            0      1440000            0 
    7000    276.16513  1440000   198783     2662    65542       0.0007     12487.89    12506.666    12494.051            0      1440000            0 
    7100    303.64527  1440000   198788     2680    65429      0.00071    12486.146    12505.341    12497.391            0      1440000            0 
    7200    331.88331  1440000   198787     2654    65208      0.00072    12495.227    12492.483    12496.744            0      1440000            0 
    7300    359.69294  1440000   198780     2643    65583      0.00073    12497.265    12488.448    12497.594            0      1440000            0 
    7400    387.52096  1440000   198787     2601    65439      0.00074    12497.553    12491.678    12494.479            0      1440000            0 
    7500    415.27582  1440000   198786     2620    65171      0.00075    12496.752    12494.075    12493.432            0      1440000            0 
    7600    443.17404  1440000   198785     2568    65157      0.00076    12491.407    12497.629    12497.252            0      1440000            0 
    7700    470.93643  1440000   198784     2718    65648      0.00077     12489.86    12493.848    12502.454            0      1440000            0 
    7800     498.7419  1440000   198787     2647    65753      0.00078    12493.135    12492.284    12499.569            0      1440000            0 
    7900    526.70191  1440000   198796     2702    65527      0.00079    12493.438    12500.186    12492.488            0      1440000            0 
    8000    554.22364  1440000   198790     2650    65535       0.0008    12495.113     12501.12    12489.557            0      1440000            0 
Loop time of 554.224 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.423     | 40.826     | 41.228     |   6.3 |  7.37
Coll    | 302.19     | 303.63     | 305.06     |   8.2 | 54.78
Sort    | 31.591     | 31.593     | 31.595     |   0.0 |  5.70
Comm    | 6.7664     | 6.801      | 6.8357     |   1.3 |  1.23
Modify  | 169.04     | 170.85     | 172.65     |  13.8 | 30.83
Output  | 0.52368    | 0.52998    | 0.53627    |   0.9 |  0.10
Other   |            | 0.001485   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3791612797 (3.79B)
Particle comms    = 41420311 (41.4M)
Boundary collides = 82833481 (82.8M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 397572261 (398M)
Collide occurs    = 130732669 (131M)
Reactions         = 5280580 (5.28M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.59823e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.31653
Particle comm iterations/step: 1
Particle fraction communicated: 0.0143821
Particle fraction colliding with boundary: 0.0287616
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.138046
Collisions/particle/step: 0.0453933
Reactions/particle/step: 0.00183353

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 5.28058e+06
    number of reactions of real particle: 3.66707e+17
    reaction rate: 1.83353e-17 (m^3/s)
    reaction rate: 1.10418e+13 (cm^3/mol/s)

Particles: 720000 ave 720992 max 719008 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    8000            0  1440000   198790     2650    65535       0.0008    12495.113     12501.12    12489.557            0      1440000            0 
    8100    27.630181  1440000   198783     2556    65641      0.00081    12494.588    12502.314    12489.206            0      1440000            0 
    8200    55.439487  1440000   198786     2510    65147      0.00082    12503.449    12497.331    12482.124            0      1440000            0 
    8300    83.039662  1440000   198784     2616    65695      0.00083     12505.27     12493.49    12483.042            0      1440000            0 
    8400    111.08412  1440000   198781     2654    65081      0.00084    12493.773    12502.337     12490.19            0      1440000            0 
    8500    138.22853  1440000   198782     2598    65205      0.00085    12490.967    12514.904     12483.09            0      1440000            0 
    8600    161.21506  1440000   198797     2688    65370      0.00086    12497.326    12506.777    12481.884            0      1440000            0 
    8700    183.41207  1440000   198789     2721    65640      0.00087    12499.857    12495.597    12488.186            0      1440000            0 
    8800     206.0146  1440000   198791     2567    64877      0.00088    12489.566    12511.024    12488.163            0      1440000            0 
    8900    228.85934  1440000   198779     2623    65525      0.00089     12486.38    12509.627    12493.493            0      1440000            0 
    9000    249.76356  1440000   198785     2699    65145       0.0009    12489.357    12511.305    12488.186            0      1440000            0 
    9100    268.12394  1440000   198795     2611    65383      0.00091    12499.582    12505.874    12479.793            0      1440000            0 
    9200    285.98106  1440000   198786     2575    65149      0.00092     12498.28    12502.624    12484.234            0      1440000            0 
    9300    303.95114  1440000   198794     2629    65326      0.00093     12494.04    12505.299    12487.355            0      1440000            0 
    9400    322.34347  1440000   198794     2691    65184      0.00094      12507.9    12495.782    12477.728            0      1440000            0 
    9500    339.06037  1440000   198791     2641    65177      0.00095    12499.564    12508.752    12477.313            0      1440000            0 
    9600    352.24604  1440000   198776     2725    65426      0.00096    12497.632    12502.519    12485.121            0      1440000            0 
    9700    364.17812  1440000   198786     2620    65116      0.00097    12494.064    12505.547    12487.104            0      1440000            0 
    9800     376.2736  1440000   198787     2630    65486      0.00098     12503.38    12492.093    12486.646            0      1440000            0 
    9900    388.08761  1440000   198785     2613    65554      0.00099    12497.137    12501.182    12486.905            0      1440000            0 
   10000     399.7413  1440000   198778     2709    65554        0.001    12497.229    12502.034    12486.065            0      1440000            0 
Loop time of 399.741 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 35.262     | 35.499     | 35.736     |   4.0 |  8.88
Coll    | 207.8      | 208.75     | 209.7      |   6.6 | 52.22
Sort    | 23.703     | 23.723     | 23.744     |   0.4 |  5.93
Comm    | 4.7986     | 4.9198     | 5.0411     |   5.5 |  1.23
Modify  | 125.61     | 126.46     | 127.31     |   7.6 | 31.64
Output  | 0.38066    | 0.38403    | 0.3874     |   0.5 |  0.10
Other   |            | 0.001178   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3791724157 (3.79B)
Particle comms    = 41430188 (41.4M)
Boundary collides = 82849975 (82.8M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 397572761 (398M)
Collide occurs    = 130746945 (131M)
Reactions         = 5283649 (5.28M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 3.60233e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.31657
Particle comm iterations/step: 1
Particle fraction communicated: 0.0143855
Particle fraction colliding with boundary: 0.0287674
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.138046
Collisions/particle/step: 0.0453982
Reactions/particle/step: 0.0018346

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 5.28365e+06
    number of reactions of real particle: 3.6692e+17
    reaction rate: 1.8346e-17 (m^3/s)
    reaction rate: 1.10482e+13 (cm^3/mol/s)

Particles: 720000 ave 720414 max 719586 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

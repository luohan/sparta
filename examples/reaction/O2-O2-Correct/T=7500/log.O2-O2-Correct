SPARTA (7 May 2020)
# test of vibrational energy modes
# thermal gas in a 3d box with collisions
# particles reflect off global box boundaries
# Tips:
# - To create particles with vibrational energy, `collide_modify vibrate discrete` needs to be ahead of create_particles
#
variable            Teq equal 7500.0
variable            mfp0  equal 0.00178   #mfp for 1000K,1e21
variable            mct   equal 2.169e-6
variable            n0    equal 1E21
variable            boxsize equal 0.01
variable            GNX0  equal ceil(${boxsize}/${mfp0})*2 # every grid is half mfp size
variable            GNX0  equal ceil(0.01/${mfp0})*2 
variable            GNX0  equal ceil(0.01/0.00178)*2 
variable            GNY0  equal ceil(${boxsize}/${mfp0})*2
variable            GNY0  equal ceil(0.01/${mfp0})*2
variable            GNY0  equal ceil(0.01/0.00178)*2
variable            FNUM  equal ${n0}*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) #every cell has 1e4 molecules
variable            FNUM  equal 1e+21*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*12*1e4) 
variable            zoomfactor equal 1/${boxsize}
variable            zoomfactor equal 1/0.01
variable            NE equal 100
variable            ND equal 100
variable            NR equal 500
variable            NMOL equal ${n0}*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/69444444444.4444
print               "NMOL=${n0}*${boxsize}*${boxsize}/${FNUM}"
NMOL=1e+21*0.01*0.01/69444444444.4444
print               "FNUM=${FNUM}"
FNUM=69444444444.4444
print               "GNX0=${GNX0} GNY0=${GNY0}"
GNX0=12 GNY0=12
print               "NMOL=${NMOL}"
NMOL=1440000
timestep            1e-7
seed	    	    1235
global              nrho ${n0} fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum 69444444444.4444 comm/sort yes
# grid info
boundary            r r p
create_box          0 ${boxsize} 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 0.01 -0.5 0.5
Created orthogonal box = (0 0 -0.5) to (0.01 0.01 0.5)
create_grid         ${GNX0} ${GNY0} 1
create_grid         12 ${GNY0} 1
create_grid         12 12 1
Created 144 child grid cells
  parent cells = 1
  CPU time = 0.000713147 secs
  create/ghost percent = 87.7215 12.2785

# surf info

# write grid
balance_grid        rcb cell
Balance grid migrated 72 cells
  CPU time = 0.000150432 secs
  reassign/sort/migrate/ghost percent = 54.3322 0.533131 21.9242 23.2105
fix                 gridcheck grid/check 1 warn
write_grid          parent Grid0_partent.grid
  parent cells = 1
  CPU time = 7.5091e-05 secs
write_grid          geom   Grid0_geom.grid
  parent cells = 1
  CPU time = 0.000933861 secs

# particle info
species		    air.species O2 O vlevelfile air_vlevel.json
mixture             background O2 frac 1.0 temp ${Teq} trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 7500 trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 7500 trot 7500 tvib ${Teq}
mixture             background O2 frac 1.0 temp 7500 trot 7500 tvib 7500
mixture             background O frac 0.0
collide             vss background air.vss relax variable isothermal no relaxtemp temp
collide_modify      rotate smooth vibrate discrete
react               tce air.tce
create_particles    background n 0
Created 1440000 particles
  CPU time = 0.583167 secs

# compute
compute             1    grid         all all n nrho massrho u v
compute             2    thermal/grid all all temp press
compute             3    tvib/grid    all species
compute             4    trot/grid    all all
compute             5    grid         all species n
variable            ftime equal dt*step

# fix
fix                 1   ave/grid     all 1 ${NE} ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 100 c_2[1] c_4[1] c_3[*] ave one
#fix                 2   ave/grid     all 1 ${NE} ${NE} c_1[*] ave one
fix                 3   ave/grid     all 1 ${NE} ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 100 c_5[*] ave one
compute             6   reduce ave   f_1[*]
compute             7   reduce sum   c_5[*]

# modify dependent on fix
# 1. use cell temperature for relaxation, this must be placed behind fix ave/grid and collide_modify of vibrate
collide_modify      temp f_1[*]
# 2. include vibrational dof in TCE, this must be placed behind collide_modify
react_modify        vdof yes frozen yes


# dump
# dump                1 grid  all ${ND} flowfield.out.* id xc yc f_1[*]
#dump               3 image all 50 dump.*.ppm type type pdiam 0.0001 view 0.0 0.0 size 1024 1024 zoom ${zoomfactor} gline yes 0.01

# stat
stats_style	    step cpu np nattempt nreact ncoll v_ftime c_6[*] c_7[*]
stats		    ${NE}
stats		    100
# restart
run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 74.25 74.25 74.25
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 75.7848 75.7848 75.7848
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
       0            0  1440000        0        0        0            0            0            0            0            0      1440000            0 
     100    24.523803  1440000   153978      204    57114        1e-05    7494.5729    7504.2728    7493.9694            0      1440000            0 
     200    49.756537  1440000   153987      228    56901        2e-05    7497.2317    7498.3236    7495.6397            0      1440000            0 
     300    75.006751  1440000   153981      220    56954        3e-05    7491.7075    7507.4055    7494.9674            0      1440000            0 
     400    100.26878  1440000   153976      229    56972        4e-05     7496.335    7501.1009    7494.4367            0      1440000            0 
     500    125.32273  1440000   153987      212    57253        5e-05    7499.1111     7499.603    7492.1712            0      1440000            0 
     600    149.74594  1440000   153980      235    57040        6e-05     7497.992    7501.5341    7491.9865            0      1440000            0 
     700    174.40085  1440000   153977      211    57028        7e-05    7496.5616    7503.5628    7492.0626            0      1440000            0 
     800     199.2391  1440000   153972      233    56603        8e-05     7500.843    7498.2816    7491.0697            0      1440000            0 
     900    223.84429  1440000   153985      232    57148        9e-05    7503.4256     7498.408    7487.7189            0      1440000            0 
    1000    248.63373  1440000   153981      226    56903       0.0001    7498.2747    7503.8147    7489.6847            0      1440000            0 
    1100    273.27081  1440000   153983      245    57254      0.00011    7498.4398    7502.1772    7490.8579            0      1440000            0 
    1200    297.76253  1440000   153974      235    57084      0.00012    7503.0881    7498.0975    7488.4159            0      1440000            0 
    1300    322.02429  1440000   153987      247    56837      0.00013    7502.9796    7501.3761    7485.7724            0      1440000            0 
    1400    346.36638  1440000   153975      232    56937      0.00014    7503.6575    7500.3225    7485.7907            0      1440000            0 
    1500    370.73681  1440000   153979      227    57090      0.00015    7509.6948    7492.3037    7484.9366            0      1440000            0 
    1600    394.99358  1440000   153970      222    56861      0.00016    7504.0337    7499.6572     7485.891            0      1440000            0 
    1700    419.52835  1440000   153977      232    56964      0.00017    7498.5347    7505.0314    7488.3203            0      1440000            0 
    1800    444.06254  1440000   153987      209    56876      0.00018    7503.0154    7497.3796    7489.0893            0      1440000            0 
    1900    468.43636  1440000   153982      216    56902      0.00019    7501.0297    7501.4844    7488.1637            0      1440000            0 
    2000    493.16986  1440000   153972      217    56858       0.0002    7498.4912    7504.3215    7488.9769            0      1440000            0 
Loop time of 493.17 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.575     | 40.617     | 40.66      |   0.7 |  8.24
Coll    | 258.83     | 259.33     | 259.83     |   3.1 | 52.58
Sort    | 34.211     | 34.279     | 34.346     |   1.2 |  6.95
Comm    | 5.212      | 5.2846     | 5.3573     |   3.2 |  1.07
Modify  | 152.86     | 153.17     | 153.49     |   2.6 | 31.06
Output  | 0.48045    | 0.48163    | 0.48281    |   0.2 |  0.10
Other   |            | 0.001153   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3586296757 (3.59B)
Particle comms    = 32086724 (32.1M)
Boundary collides = 64182221 (64.2M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 307958394 (308M)
Collide occurs    = 113997968 (114M)
Reactions         = 445946 (0.446M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.91989e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.24524
Particle comm iterations/step: 1
Particle fraction communicated: 0.0111412
Particle fraction colliding with boundary: 0.0222855
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.10693
Collisions/particle/step: 0.0395826
Reactions/particle/step: 0.000154842

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 445946
    number of reactions of real particle: 3.09685e+16
    reaction rate: 1.54842e-18 (m^3/s)
    reaction rate: 9.32482e+11 (cm^3/mol/s)

Particles: 720000 ave 720770 max 719230 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    2000            0  1440000   153972      217    56858       0.0002    7498.4912    7504.3215    7488.9769            0      1440000            0 
    2100    24.693052  1440000   153975      227    56944      0.00021    7504.0474    7494.7277     7490.041            0      1440000            0 
    2200     48.94177  1440000   153982      227    56851      0.00022    7501.5976    7499.1062    7489.4287            0      1440000            0 
    2300    73.384044  1440000   153976      221    57173      0.00023    7496.9745    7508.7075    7487.1747            0      1440000            0 
    2400    97.842888  1440000   153975      219    57084      0.00024     7500.264    7504.2333    7486.7907            0      1440000            0 
    2500    122.20099  1440000   153975      234    56974      0.00025    7494.2648    7510.9831    7488.7011            0      1440000            0 
    2600     147.0209  1440000   153981      219    57187      0.00026     7497.933    7505.1467    7488.9903            0      1440000            0 
    2700    171.60669  1440000   153980      201    57143      0.00027    7500.1684    7503.3581    7487.6464            0      1440000            0 
    2800    196.46687  1440000   153983      232    56899      0.00028    7500.8363    7501.8356     7488.097            0      1440000            0 
    2900    221.03171  1440000   153975      223    57026      0.00029    7498.2896    7503.3271    7490.0644            0      1440000            0 
    3000    246.08897  1440000   153979      230    57142       0.0003    7494.1212    7509.5788    7490.0964            0      1440000            0 
    3100    271.18327  1440000   153979      190    57006      0.00031    7499.6726    7501.3138    7490.0026            0      1440000            0 
    3200    296.11697  1440000   153977      227    57046      0.00032    7500.5334    7500.3262    7489.7368            0      1440000            0 
    3300    320.77977  1440000   153979      242    56853      0.00033    7502.7279    7498.2986    7488.6727            0      1440000            0 
    3400    344.96568  1440000   153984      211    56913      0.00034    7502.6283    7497.0898    7489.8515            0      1440000            0 
    3500    369.92996  1440000   153974      219    56806      0.00035    7497.4889    7504.3606    7490.1892            0      1440000            0 
    3600    394.77555  1440000   153985      225    57237      0.00036    7496.1337    7506.8803    7489.7797            0      1440000            0 
    3700    419.93043  1440000   153979      189    57416      0.00037    7500.8954    7500.8561    7488.8486            0      1440000            0 
    3800    444.38746  1440000   153979      221    56939      0.00038    7497.2197     7506.742    7488.5518            0      1440000            0 
    3900    469.41551  1440000   153980      216    57500      0.00039    7501.1974    7502.3543    7487.1992            0      1440000            0 
    4000    493.77325  1440000   153981      242    57222       0.0004    7502.3791    7501.2108    7486.6559            0      1440000            0 
Loop time of 493.773 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.27      | 40.289     | 40.309     |   0.3 |  8.16
Coll    | 259.7      | 261.44     | 263.17     |  10.7 | 52.95
Sort    | 34.337     | 34.414     | 34.491     |   1.3 |  6.97
Comm    | 5.2085     | 5.301      | 5.3934     |   4.0 |  1.07
Modify  | 150.37     | 151.92     | 153.46     |  12.5 | 30.77
Output  | 0.40767    | 0.40947    | 0.41126    |   0.3 |  0.08
Other   |            | 0.001144   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3586262588 (3.59B)
Particle comms    = 32087176 (32.1M)
Boundary collides = 64178124 (64.2M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 307958466 (308M)
Collide occurs    = 113993959 (114M)
Reactions         = 446092 (0.446M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.91632e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.24523
Particle comm iterations/step: 1
Particle fraction communicated: 0.0111414
Particle fraction colliding with boundary: 0.0222841
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.10693
Collisions/particle/step: 0.0395812
Reactions/particle/step: 0.000154893

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 446092
    number of reactions of real particle: 3.09786e+16
    reaction rate: 1.54893e-18 (m^3/s)
    reaction rate: 9.32788e+11 (cm^3/mol/s)

Particles: 720000 ave 720162 max 719838 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    4000            0  1440000   153981      242    57222       0.0004    7502.3791    7501.2108    7486.6559            0      1440000            0 
    4100     24.34148  1440000   153991      243    57406      0.00041    7497.1853    7508.9943    7486.6823            0      1440000            0 
    4200     48.94664  1440000   153984      237    57198      0.00042    7497.8089    7505.4396    7488.8752            0      1440000            0 
    4300    73.861164  1440000   153979      224    56779      0.00043    7500.7811    7498.5433    7490.9413            0      1440000            0 
    4400     98.77132  1440000   153979      221    57038      0.00044    7500.2951    7499.6788    7490.5953            0      1440000            0 
    4500    123.50762  1440000   153978      229    57066      0.00045    7497.2074    7504.3298    7490.5801            0      1440000            0 
    4600    148.38209  1440000   153975      240    57222      0.00046    7495.2355    7507.3643    7490.5184            0      1440000            0 
    4700    173.37891  1440000   153974      189    57163      0.00047    7496.9361    7506.9682     7488.712            0      1440000            0 
    4800    198.12716  1440000   153985      243    57025      0.00048    7496.3553    7508.3782    7488.2263            0      1440000            0 
    4900    222.94029  1440000   153981      227    57254      0.00049    7497.8608    7503.0025      7490.88            0      1440000            0 
    5000    247.98674  1440000   153973      248    57497       0.0005    7500.6271    7498.0331    7491.6088            0      1440000            0 
    5100    272.96669  1440000   153979      230    56871      0.00051    7499.1899    7502.5094    7489.6085            0      1440000            0 
    5200    297.48405  1440000   153977      236    56636      0.00052    7499.2513    7503.6111      7488.61            0      1440000            0 
    5300    322.22551  1440000   153978      235    57070      0.00053    7498.9047    7504.3977    7488.3983            0      1440000            0 
    5400    346.63752  1440000   153988      234    56709      0.00054    7500.5241    7501.7827     7488.534            0      1440000            0 
    5500    371.42618  1440000   153977      191    57052      0.00055    7499.7335    7502.0726    7489.2949            0      1440000            0 
    5600    395.94647  1440000   153978      236    57024      0.00056    7500.6087     7501.314    7488.8244            0      1440000            0 
    5700    420.98692  1440000   153984      210    57204      0.00057    7499.6288    7502.8527    7488.7596            0      1440000            0 
    5800    445.64388  1440000   153984      243    56941      0.00058    7498.3827    7506.3737     7487.387            0      1440000            0 
    5900    470.24327  1440000   153973      235    57178      0.00059    7499.2055    7505.8403    7486.7839            0      1440000            0 
    6000    494.90464  1440000   153978      214    56979       0.0006    7500.0842    7507.6339    7484.1563            0      1440000            0 
Loop time of 494.905 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.592     | 40.631     | 40.67      |   0.6 |  8.21
Coll    | 259.84     | 260.11     | 260.38     |   1.7 | 52.56
Sort    | 34.17      | 34.238     | 34.307     |   1.2 |  6.92
Comm    | 5.1445     | 5.2886     | 5.4327     |   6.3 |  1.07
Modify  | 154.24     | 154.26     | 154.28     |   0.1 | 31.17
Output  | 0.37201    | 0.3751     | 0.37819    |   0.5 |  0.08
Other   |            | 0.001154   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3586169588 (3.59B)
Particle comms    = 32086558 (32.1M)
Boundary collides = 64170127 (64.2M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 307958530 (308M)
Collide occurs    = 114012386 (114M)
Reactions         = 444927 (0.445M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.90965e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.2452
Particle comm iterations/step: 1
Particle fraction communicated: 0.0111412
Particle fraction colliding with boundary: 0.0222813
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.10693
Collisions/particle/step: 0.0395876
Reactions/particle/step: 0.000154489

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 444927
    number of reactions of real particle: 3.08977e+16
    reaction rate: 1.54489e-18 (m^3/s)
    reaction rate: 9.30352e+11 (cm^3/mol/s)

Particles: 720000 ave 720986 max 719014 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    6000            0  1440000   153978      214    56979       0.0006    7500.0842    7507.6339    7484.1563            0      1440000            0 
    6100    24.713181  1440000   153969      200    57117      0.00061    7495.8532    7514.1247    7484.0315            0      1440000            0 
    6200    49.865593  1440000   153974      220    57234      0.00062    7498.7483    7510.7259     7483.221            0      1440000            0 
    6300    74.639937  1440000   153979      202    57314      0.00063    7502.6116    7504.5008    7483.5985            0      1440000            0 
    6400    99.343032  1440000   153974      228    57336      0.00064    7502.8633    7504.4171    7483.3377            0      1440000            0 
    6500     123.7712  1440000   153979      272    57205      0.00065    7503.3324    7504.3868    7482.7739            0      1440000            0 
    6600    148.32002  1440000   153982      216    57063      0.00066    7505.9298    7500.7791    7482.5423            0      1440000            0 
    6700    172.68479  1440000   153982      230    57197      0.00067    7505.9583     7502.197    7481.3022            0      1440000            0 
    6800    196.93093  1440000   153977      208    57282      0.00068     7505.051    7503.6471    7481.2195            0      1440000            0 
    6900    221.09311  1440000   153987      245    57116      0.00069    7501.4055    7506.2466    7483.6227            0      1440000            0 
    7000    245.63163  1440000   153977      209    56758       0.0007      7502.18    7505.6683    7483.1444            0      1440000            0 
    7100    270.20537  1440000   153981      225    56729      0.00071    7503.3958    7503.7438    7483.2313            0      1440000            0 
    7200    294.50997  1440000   153974      193    56557      0.00072    7502.3866    7503.4174    7484.8015            0      1440000            0 
    7300    318.96779  1440000   153974      206    57415      0.00073    7503.1704    7501.9313    7485.0608            0      1440000            0 
    7400    343.75536  1440000   153972      183    56849      0.00074    7502.9832    7501.3926    7485.7584            0      1440000            0 
    7500    368.30366  1440000   153976      239    57128      0.00075    7501.4873    7502.8828    7486.4003            0      1440000            0 
    7600    392.68804  1440000   153978      210    56939      0.00076    7498.9566     7507.093    7486.0541            0      1440000            0 
    7700    417.16091  1440000   153988      224    57067      0.00077    7500.3737    7503.0625    7487.6644            0      1440000            0 
    7800    441.66273  1440000   153979      230    56785      0.00078    7505.2574    7495.3713    7487.9572            0      1440000            0 
    7900    466.09987  1440000   153982      229    56868      0.00079    7500.4815    7499.5361    7490.4901            0      1440000            0 
    8000    490.94021  1440000   153988      234    57161       0.0008    7497.7829    7501.3964    7492.3288            0      1440000            0 
Loop time of 490.94 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 40.571     | 40.624     | 40.678     |   0.8 |  8.27
Coll    | 257.08     | 257.74     | 258.4      |   4.1 | 52.50
Sort    | 34.179     | 34.262     | 34.344     |   1.4 |  6.98
Comm    | 5.2479     | 5.3249     | 5.4019     |   3.3 |  1.08
Modify  | 151.64     | 152.52     | 153.39     |   7.1 | 31.07
Output  | 0.46659    | 0.47172    | 0.47685    |   0.7 |  0.10
Other   |            | 0.001167   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3586349692 (3.59B)
Particle comms    = 32092288 (32.1M)
Boundary collides = 64180136 (64.2M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 307958335 (308M)
Collide occurs    = 114013293 (114M)
Reactions         = 445073 (0.445M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.93315e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.24526
Particle comm iterations/step: 1
Particle fraction communicated: 0.0111432
Particle fraction colliding with boundary: 0.0222848
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.10693
Collisions/particle/step: 0.0395879
Reactions/particle/step: 0.000154539

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 445073
    number of reactions of real particle: 3.09078e+16
    reaction rate: 1.54539e-18 (m^3/s)
    reaction rate: 9.30657e+11 (cm^3/mol/s)

Particles: 720000 ave 720348 max 719652 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    8000            0  1440000   153988      234    57161       0.0008    7497.7829    7501.3964    7492.3288            0      1440000            0 
    8100    25.117551  1440000   153983      207    57055      0.00081    7498.1312    7500.3387    7492.7802            0      1440000            0 
    8200     50.20968  1440000   153969      230    57001      0.00082    7498.3813     7500.327    7492.4671            0      1440000            0 
    8300    74.942664  1440000   153978      234    57269      0.00083    7495.6413    7505.6965    7491.4522            0      1440000            0 
    8400    100.07497  1440000   153975      233    56919      0.00084    7498.4914    7502.8211    7490.2338            0      1440000            0 
    8500    124.79222  1440000   153961      207    56826      0.00085    7503.8349    7495.1662    7489.9441            0      1440000            0 
    8600    149.32486  1440000   153975      224    57060      0.00086    7502.1133    7496.0744     7491.335            0      1440000            0 
    8700     173.7493  1440000   153977      233    56751      0.00087    7497.9889    7501.2568    7492.1987            0      1440000            0 
    8800    198.48645  1440000   153986      242    57190      0.00088    7495.6446    7504.6174    7492.3205            0      1440000            0 
    8900    223.59942  1440000   153973      237    56882      0.00089    7496.2752    7503.7875    7492.2255            0      1440000            0 
    9000    248.52301  1440000   153990      239    57027       0.0009    7492.0131    7511.4652    7491.1346            0      1440000            0 
    9100    273.73713  1440000   153973      218    57062      0.00091    7496.1699    7507.1781    7489.4854            0      1440000            0 
    9200    298.54747  1440000   153974      215    56870      0.00092    7498.7353    7502.5572    7490.1528            0      1440000            0 
    9300    323.94797  1440000   153981      228    56886      0.00093    7497.0884    7506.8289     7488.614            0      1440000            0 
    9400     349.1593  1440000   153976      208    57477      0.00094    7494.8112    7510.3641    7488.5247            0      1440000            0 
    9500    372.66212  1440000   153978      215    56985      0.00095    7495.1898    7508.8568    7489.3377            0      1440000            0 
    9600    392.78731  1440000   153969      245    56866      0.00096    7497.3406    7507.2559    7487.9396            0      1440000            0 
    9700     412.9586  1440000   153981      216    56738      0.00097    7500.9572    7501.0791    7488.5864            0      1440000            0 
    9800    432.73222  1440000   153980      253    57392      0.00098    7504.6422    7497.5267    7486.9228            0      1440000            0 
    9900    452.28019  1440000   153978      203    56992      0.00099    7503.6438    7500.6532    7485.5397            0      1440000            0 
   10000    471.98715  1440000   153983      245    56786        0.001    7508.2116    7494.1256    7485.2599            0      1440000            0 
Loop time of 471.987 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 39.254     | 39.311     | 39.368     |   0.9 |  8.33
Coll    | 248.3      | 248.63     | 248.95     |   2.1 | 52.68
Sort    | 32.87      | 32.933     | 32.995     |   1.1 |  6.98
Comm    | 5.0099     | 5.054      | 5.0981     |   2.0 |  1.07
Modify  | 145.1      | 145.59     | 146.08     |   4.1 | 30.85
Output  | 0.46636    | 0.46686    | 0.46735    |   0.1 |  0.10
Other   |            | 0.00112    |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3586253530 (3.59B)
Particle comms    = 32083279 (32.1M)
Boundary collides = 64183656 (64.2M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 307958467 (308M)
Collide occurs    = 114005779 (114M)
Reactions         = 444809 (0.445M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 3.05093e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.24523
Particle comm iterations/step: 1
Particle fraction communicated: 0.01114
Particle fraction colliding with boundary: 0.022286
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.10693
Collisions/particle/step: 0.0395853
Reactions/particle/step: 0.000154448

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 444809
    number of reactions of real particle: 3.08895e+16
    reaction rate: 1.54448e-18 (m^3/s)
    reaction rate: 9.30105e+11 (cm^3/mol/s)

Particles: 720000 ave 720575 max 719425 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

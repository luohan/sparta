SPARTA (7 May 2020)
# test of vibrational energy modes
# thermal gas in a 3d box with collisions
# particles reflect off global box boundaries
# Tips:
# - To create particles with vibrational energy, `collide_modify vibrate discrete` needs to be ahead of create_particles
#
variable            Teq equal 10000.0
variable            mfp0  equal 0.00178   #mfp for 1000K,1e21
variable            mct   equal 2.169e-6
variable            n0    equal 1E21
variable            boxsize equal 0.01
variable            GNX0  equal ceil(${boxsize}/${mfp0})*2 # every grid is half mfp size
variable            GNX0  equal ceil(0.01/${mfp0})*2 
variable            GNX0  equal ceil(0.01/0.00178)*2 
variable            GNY0  equal ceil(${boxsize}/${mfp0})*2
variable            GNY0  equal ceil(0.01/${mfp0})*2
variable            GNY0  equal ceil(0.01/0.00178)*2
variable            FNUM  equal ${n0}*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) #every cell has 1e4 molecules
variable            FNUM  equal 1e+21*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*${boxsize}/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(${GNX0}*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*${GNY0}*1e4) 
variable            FNUM  equal 1e+21*0.01*0.01/(12*12*1e4) 
variable            zoomfactor equal 1/${boxsize}
variable            zoomfactor equal 1/0.01
variable            NE equal 100
variable            ND equal 100
variable            NR equal 500
variable            NMOL equal ${n0}*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*${boxsize}*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*${boxsize}/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/${FNUM}
variable            NMOL equal 1e+21*0.01*0.01/69444444444.4444
print               "NMOL=${n0}*${boxsize}*${boxsize}/${FNUM}"
NMOL=1e+21*0.01*0.01/69444444444.4444
print               "FNUM=${FNUM}"
FNUM=69444444444.4444
print               "GNX0=${GNX0} GNY0=${GNY0}"
GNX0=12 GNY0=12
print               "NMOL=${NMOL}"
NMOL=1440000
timestep            1e-7
seed	    	    1235
global              nrho ${n0} fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum ${FNUM} comm/sort yes
global              nrho 1e+21 fnum 69444444444.4444 comm/sort yes
# grid info
boundary            r r p
create_box          0 ${boxsize} 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 ${boxsize} -0.5 0.5
create_box          0 0.01 0 0.01 -0.5 0.5
Created orthogonal box = (0 0 -0.5) to (0.01 0.01 0.5)
create_grid         ${GNX0} ${GNY0} 1
create_grid         12 ${GNY0} 1
create_grid         12 12 1
Created 144 child grid cells
  parent cells = 1
  CPU time = 0.000633388 secs
  create/ghost percent = 86.9661 13.0339

# surf info

# write grid
balance_grid        rcb cell
Balance grid migrated 72 cells
  CPU time = 0.000145303 secs
  reassign/sort/migrate/ghost percent = 52.5894 0.454911 22.7401 24.2156
fix                 gridcheck grid/check 1 warn
write_grid          parent Grid0_partent.grid
  parent cells = 1
  CPU time = 0.000109926 secs
write_grid          geom   Grid0_geom.grid
  parent cells = 1
  CPU time = 0.000934482 secs

# particle info
species		    air.species O2 O vlevelfile air_vlevel.json
mixture             background O2 frac 1.0 temp ${Teq} trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 10000 trot ${Teq} tvib ${Teq}
mixture             background O2 frac 1.0 temp 10000 trot 10000 tvib ${Teq}
mixture             background O2 frac 1.0 temp 10000 trot 10000 tvib 10000
mixture             background O frac 0.0
collide             vss background air.vss relax variable isothermal no relaxtemp temp
collide_modify      rotate smooth vibrate discrete
react               tce air.tce
create_particles    background n 0
Created 1440000 particles
  CPU time = 0.585628 secs

# compute
compute             1    grid         all all n nrho massrho u v
compute             2    thermal/grid all all temp press
compute             3    tvib/grid    all species
compute             4    trot/grid    all all
compute             5    grid         all species n
variable            ftime equal dt*step

# fix
fix                 1   ave/grid     all 1 ${NE} ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 1   ave/grid     all 1 100 100 c_2[1] c_4[1] c_3[*] ave one
#fix                 2   ave/grid     all 1 ${NE} ${NE} c_1[*] ave one
fix                 3   ave/grid     all 1 ${NE} ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 ${NE} c_5[*] ave one
fix                 3   ave/grid     all 1 100 100 c_5[*] ave one
compute             6   reduce ave   f_1[*]
compute             7   reduce sum   c_5[*]

# modify dependent on fix
# 1. use cell temperature for relaxation, this must be placed behind fix ave/grid and collide_modify of vibrate
collide_modify      temp f_1[*]
# 2. include vibrational dof in TCE, this must be placed behind collide_modify
react_modify        vdof yes frozen yes


# dump
# dump                1 grid  all ${ND} flowfield.out.* id xc yc f_1[*]
#dump               3 image all 50 dump.*.ppm type type pdiam 0.0001 view 0.0 0.0 size 1024 1024 zoom ${zoomfactor} gline yes 0.01

# stat
stats_style	    step cpu np nattempt nreact ncoll v_ftime c_6[*] c_7[*]
stats		    ${NE}
stats		    100
# restart
run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 74.25 74.25 74.25
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 75.7848 75.7848 75.7848
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
       0            0  1440000        0        0        0            0            0            0            0            0      1440000            0 
     100    25.840567  1440000   177797     1327    61532        1e-05    9990.4622    10012.563    9989.3158            0      1440000            0 
     200    52.084572  1440000   177793     1294    61967        2e-05    9991.1485    10012.304    9988.6327            0      1440000            0 
     300    78.250192  1440000   177807     1390    61845        3e-05     9992.943    10010.529    9987.9314            0      1440000            0 
     400    104.82922  1440000   177791     1349    61366        4e-05    9993.1599    10008.588    9989.2419            0      1440000            0 
     500    130.87259  1440000   177795     1348    61351        5e-05    10001.039    10000.141    9986.5134            0      1440000            0 
     600     156.6539  1440000   177804     1367    61976        6e-05     10007.23    9996.1079    9982.2254            0      1440000            0 
     700    182.12013  1440000   177799     1296    61210        7e-05    10003.314    10003.257    9981.1827            0      1440000            0 
     800    207.70345  1440000   177801     1275    61433        8e-05    9993.9463    10015.313    9982.8184            0      1440000            0 
     900    233.85138  1440000   177808     1321    61650        9e-05    9999.8122    9998.9875    9988.9497            0      1440000            0 
    1000    260.20078  1440000   177796     1349    61589       0.0001    10001.504    9994.3499     9990.642            0      1440000            0 
    1100    286.08395  1440000   177796     1330    61604      0.00011    10003.637    9996.3364    9986.4272            0      1440000            0 
    1200    311.51659  1440000   177805     1327    61448      0.00012    10000.915    10002.141    9985.0265            0      1440000            0 
    1300    337.01279  1440000   177799     1365    61432      0.00013    10005.278    9989.9643     9989.578            0      1440000            0 
    1400     363.0467  1440000   177800     1284    61248      0.00014    10000.596    9994.4502    9991.6563            0      1440000            0 
    1500    388.79454  1440000   177806     1293    61478      0.00015    9997.7819    10000.865    9989.8668            0      1440000            0 
    1600    414.44848  1440000   177795     1309    61601      0.00016    10004.967    9989.6198    9990.2629            0      1440000            0 
    1700     440.3105  1440000   177794     1328    61520      0.00017    10006.723    9988.7776    9988.8056            0      1440000            0 
    1800    466.00899  1440000   177794     1358    61867      0.00018    9999.6462     9995.732    9991.7883            0      1440000            0 
    1900     492.7253  1440000   177796     1320    61758      0.00019    10004.316    9988.6615     9991.827            0      1440000            0 
    2000    519.22092  1440000   177804     1249    61549       0.0002    10001.239    9995.2066    9990.2435            0      1440000            0 
Loop time of 519.221 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 41.282     | 41.463     | 41.643     |   2.8 |  7.99
Coll    | 277.66     | 277.84     | 278.02     |   1.1 | 53.51
Sort    | 33.997     | 34.001     | 34.005     |   0.1 |  6.55
Comm    | 6.1943     | 6.449      | 6.7037     |  10.0 |  1.24
Modify  | 158.71     | 158.98     | 159.24     |   2.1 | 30.62
Output  | 0.48464    | 0.48972    | 0.4948     |   0.7 |  0.09
Other   |            | 0.001483   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3695533213 (3.7B)
Particle comms    = 37054528 (37.1M)
Boundary collides = 74103210 (74.1M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 355599535 (356M)
Collide occurs    = 123155301 (123M)
Reactions         = 2648068 (2.65M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.77339e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.28317
Particle comm iterations/step: 1
Particle fraction communicated: 0.0128662
Particle fraction colliding with boundary: 0.0257303
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.123472
Collisions/particle/step: 0.0427623
Reactions/particle/step: 0.000919468

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 2.64807e+06
    number of reactions of real particle: 1.83894e+17
    reaction rate: 9.19468e-18 (m^3/s)
    reaction rate: 5.53717e+12 (cm^3/mol/s)

Particles: 720000 ave 720634 max 719366 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    2000            0  1440000   177804     1249    61549       0.0002    10001.239    9995.2066    9990.2435            0      1440000            0 
    2100    26.012264  1440000   177807     1296    61493      0.00021    10003.118    9992.4558    9990.2152            0      1440000            0 
    2200    51.706319  1440000   177795     1336    61514      0.00022     9995.407    10003.083    9990.9777            0      1440000            0 
    2300    77.661828  1440000   177808     1343    61473      0.00023     10003.45    9991.7639    9990.3633            0      1440000            0 
    2400    103.49934  1440000   177790     1354    61484      0.00024    10006.228    9993.0246    9985.9498            0      1440000            0 
    2500      129.373  1440000   177800     1362    61729      0.00025    9999.3096    10001.376    9987.5868            0      1440000            0 
    2600    155.78515  1440000   177793     1309    61484      0.00026    9998.6598    9999.6812    9989.7843            0      1440000            0 
    2700     181.5786  1440000   177807     1271    61652      0.00027    9993.2644    10007.593    9989.9202            0      1440000            0 
    2800    207.66446  1440000   177799     1303    61357      0.00028    9995.6573    10002.143    9991.4439            0      1440000            0 
    2900    233.80921  1440000   177799     1315    61645      0.00029    9993.5004    10000.126    9995.6793            0      1440000            0 
    3000    260.46258  1440000   177810     1233    61505       0.0003    9993.5301    10000.348    9995.4676            0      1440000            0 
    3100     287.3516  1440000   177797     1286    61391      0.00031    9988.9497    9997.9385    10002.983            0      1440000            0 
    3200    314.25321  1440000   177798     1287    61451      0.00032    9989.9228    9997.9414    10001.841            0      1440000            0 
    3300    341.49472  1440000   177793     1282    61623      0.00033    9993.7641    9990.1592    10003.482            0      1440000            0 
    3400     368.7376  1440000   177811     1346    61735      0.00034    9991.3202    9998.6866    9999.5135            0      1440000            0 
    3500    396.26282  1440000   177806     1345    61461      0.00035    9991.0485    10002.421    9996.8219            0      1440000            0 
    3600    422.99245  1440000   177798     1346    61618      0.00036    9998.3592     9992.558     9995.926            0      1440000            0 
    3700    449.93751  1440000   177797     1354    61995      0.00037    9995.0093     9996.599    9996.7038            0      1440000            0 
    3800    476.59806  1440000   177804     1255    61639      0.00038    9998.4658    9994.3939    9994.2996            0      1440000            0 
    3900    503.30583  1440000   177799     1267    61672      0.00039    9998.6329    9994.6964     9993.868            0      1440000            0 
    4000    529.77338  1440000   177793     1376    61663       0.0004    10002.596    9991.4793     9991.644            0      1440000            0 
Loop time of 529.773 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 42.073     | 42.264     | 42.454     |   2.9 |  7.98
Coll    | 283.54     | 284.18     | 284.81     |   3.8 | 53.64
Sort    | 35.448     | 35.462     | 35.476     |   0.2 |  6.69
Comm    | 6.4091     | 6.6456     | 6.8821     |   9.2 |  1.25
Modify  | 159.55     | 160.63     | 161.71     |   8.5 | 30.32
Output  | 0.58815    | 0.59524    | 0.60233    |   0.9 |  0.11
Other   |            | 0.001487   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3695409973 (3.7B)
Particle comms    = 37046393 (37M)
Boundary collides = 74091341 (74.1M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 355599670 (356M)
Collide occurs    = 123145214 (123M)
Reactions         = 2644645 (2.64M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.71814e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.28313
Particle comm iterations/step: 1
Particle fraction communicated: 0.0128633
Particle fraction colliding with boundary: 0.0257262
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.123472
Collisions/particle/step: 0.0427588
Reactions/particle/step: 0.00091828

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 2.64464e+06
    number of reactions of real particle: 1.83656e+17
    reaction rate: 9.1828e-18 (m^3/s)
    reaction rate: 5.53001e+12 (cm^3/mol/s)

Particles: 720000 ave 720475 max 719525 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    4000            0  1440000   177793     1376    61663       0.0004    10002.596    9991.4793     9991.644            0      1440000            0 
    4100    26.645823  1440000   177788     1339    61738      0.00041    9994.6616    10002.341    9992.4925            0      1440000            0 
    4200    53.218721  1440000   177801     1326    61548      0.00042    9995.6674    10003.223    9990.5638            0      1440000            0 
    4300    79.592284  1440000   177802     1377    61613      0.00043    9994.7065    10004.414    9990.7283            0      1440000            0 
    4400    106.22182  1440000   177790     1309    61797      0.00044    9998.0205    10000.482     9989.881            0      1440000            0 
    4500    132.15056  1440000   177790     1308    61646      0.00045    10005.146    9993.5277    9986.8742            0      1440000            0 
    4600    158.36603  1440000   177791     1339    61332      0.00046    9999.4122    10003.014     9986.138            0      1440000            0 
    4700    184.28344  1440000   177788     1300    61934      0.00047     9999.181    10001.295    9987.8225            0      1440000            0 
    4800     210.3424  1440000   177802     1349    61568      0.00048    9999.4585    10000.813    9987.8889            0      1440000            0 
    4900    236.29099  1440000   177793     1343    61517      0.00049     10005.37    9994.2459    9986.0068            0      1440000            0 
    5000    262.04096  1440000   177804     1397    61697       0.0005    10000.968    9995.6482    9990.2459            0      1440000            0 
    5100    287.93653  1440000   177797     1288    61395      0.00051    9994.3308    10000.918    9994.0304            0      1440000            0 
    5200    314.69044  1440000   177794     1295    61702      0.00052    9997.7876    9998.8841    9991.4854            0      1440000            0 
    5300    341.06755  1440000   177808     1351    61038      0.00053    9993.2721    10006.193    9991.0256            0      1440000            0 
    5400    367.41173  1440000   177801     1297    61488      0.00054    9995.4879    10004.154    9990.0033            0      1440000            0 
    5500    393.85698  1440000   177789     1304    61966      0.00055    9992.6442    10003.112    9994.3027            0      1440000            0 
    5600    420.33899  1440000   177798     1303    61883      0.00056    9989.8027    10006.049    9995.3992            0      1440000            0 
    5700    447.38586  1440000   177807     1265    61710      0.00057      9994.79    10003.764    9991.1665            0      1440000            0 
    5800    473.99099  1440000   177805     1362    61740      0.00058    9997.5013     10002.63    9988.7605            0      1440000            0 
    5900    500.25051  1440000   177789     1314    61401      0.00059    9997.0652     10006.38    9986.2458            0      1440000            0 
    6000    526.74826  1440000   177805     1309    60889       0.0006    10004.667     9995.737    9985.6527            0      1440000            0 
Loop time of 526.748 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 41.859     | 42.027     | 42.194     |   2.6 |  7.98
Coll    | 280.22     | 280.27     | 280.32     |   0.3 | 53.21
Sort    | 35.173     | 35.192     | 35.21      |   0.3 |  6.68
Comm    | 6.271      | 6.5936     | 6.9162     |  12.6 |  1.25
Modify  | 161.63     | 162.19     | 162.75     |   4.4 | 30.79
Output  | 0.46897    | 0.474      | 0.47904    |   0.7 |  0.09
Other   |            | 0.001485   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3695504044 (3.7B)
Particle comms    = 37054983 (37.1M)
Boundary collides = 74097923 (74.1M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 355599701 (356M)
Collide occurs    = 123142026 (123M)
Reactions         = 2644610 (2.64M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.73375e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.28316
Particle comm iterations/step: 1
Particle fraction communicated: 0.0128663
Particle fraction colliding with boundary: 0.0257284
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.123472
Collisions/particle/step: 0.0427576
Reactions/particle/step: 0.000918267

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 2.64461e+06
    number of reactions of real particle: 1.83653e+17
    reaction rate: 9.18267e-18 (m^3/s)
    reaction rate: 5.52994e+12 (cm^3/mol/s)

Particles: 720000 ave 720436 max 719564 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    6000            0  1440000   177805     1309    60889       0.0006    10004.667     9995.737    9985.6527            0      1440000            0 
    6100    25.970881  1440000   177806     1352    61501      0.00061    9996.3814    10002.587    9990.1889            0      1440000            0 
    6200    52.333775  1440000   177792     1354    61683      0.00062    10000.758    9994.5544    9991.3864            0      1440000            0 
    6300     78.59064  1440000   177795     1356    61453      0.00063    9998.6378    9996.7096    9992.1983            0      1440000            0 
    6400    104.79349  1440000   177804     1341    61612      0.00064    9997.1442    9999.4506    9991.8161            0      1440000            0 
    6500    131.43496  1440000   177799     1274    61436      0.00065    9993.2931    10002.305    9994.1795            0      1440000            0 
    6600    158.11306  1440000   177796     1341    61745      0.00066    9996.2149    10000.486    9992.0856            0      1440000            0 
    6700    184.38719  1440000   177808     1309    61673      0.00067     9992.937    10008.309    9989.7282            0      1440000            0 
    6800    210.93644  1440000   177802     1393    61582      0.00068    9994.0123    10008.463    9988.3003            0      1440000            0 
    6900    237.36929  1440000   177796     1344    61437      0.00069    9989.9747    10013.154    9989.4132            0      1440000            0 
    7000     263.7348  1440000   177800     1294    61596       0.0007    9995.1096    10004.269    9990.3954            0      1440000            0 
    7100    290.23227  1440000   177804     1273    61543      0.00071    9998.3675     10006.74    9984.4046            0      1440000            0 
    7200     316.2557  1440000   177793     1386    62135      0.00072    9999.3267    10004.788    9984.8309            0      1440000            0 
    7300     342.3335  1440000   177796     1299    61356      0.00073    9998.5511    10003.822    9986.5571            0      1440000            0 
    7400    368.47475  1440000   177798     1328    61557      0.00074    10001.226    10001.573    9985.0929            0      1440000            0 
    7500    394.14891  1440000   177804     1254    61801      0.00075    9998.4004    10003.245    9987.1772            0      1440000            0 
    7600    420.22091  1440000   177797     1332    61635      0.00076    10001.917    9997.9345    9987.2372            0      1440000            0 
    7700    446.20533  1440000   177808     1379    61798      0.00077    9996.3065    10001.069    9991.4847            0      1440000            0 
    7800    472.30571  1440000   177802     1317    61991      0.00078    9994.4206    10001.619    9993.3825            0      1440000            0 
    7900    498.65799  1440000   177804     1333    61272      0.00079    9992.9151    10002.619    9994.3779            0      1440000            0 
    8000    525.17236  1440000   177806     1318    61447       0.0008    9995.6276    10000.007    9993.2138            0      1440000            0 
Loop time of 525.172 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 41.59      | 41.733     | 41.875     |   2.2 |  7.95
Coll    | 281.06     | 281.82     | 282.58     |   4.5 | 53.66
Sort    | 34.914     | 34.917     | 34.919     |   0.0 |  6.65
Comm    | 6.118      | 6.4806     | 6.8431     |  14.2 |  1.23
Modify  | 159.53     | 159.78     | 160.04     |   2.0 | 30.42
Output  | 0.43486    | 0.43845    | 0.44204    |   0.5 |  0.08
Other   |            | 0.001467   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3695425910 (3.7B)
Particle comms    = 37046007 (37M)
Boundary collides = 74097955 (74.1M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 355599647 (356M)
Collide occurs    = 123140900 (123M)
Reactions         = 2646763 (2.65M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 2.74196e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.28313
Particle comm iterations/step: 1
Particle fraction communicated: 0.0128632
Particle fraction colliding with boundary: 0.0257285
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.123472
Collisions/particle/step: 0.0427573
Reactions/particle/step: 0.000919015

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 2.64676e+06
    number of reactions of real particle: 1.83803e+17
    reaction rate: 9.19015e-18 (m^3/s)
    reaction rate: 5.53444e+12 (cm^3/mol/s)

Particles: 720000 ave 720905 max 719095 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

run 		    2000
Memory usage per proc in Mbytes:
  particles (ave,min,max) = 75.9375 75.9375 75.9375
  grid      (ave,min,max) = 1.51388 1.51388 1.51388
  surf      (ave,min,max) = 0 0 0
  total     (ave,min,max) = 77.4723 77.4723 77.4723
Step CPU Np Natt Nreact Ncoll v_ftime c_6[1] c_6[2] c_6[3] c_6[4] c_7[1] c_7[2] 
    8000            0  1440000   177806     1318    61447       0.0008    9995.6276    10000.007    9993.2138            0      1440000            0 
    8100    26.195289  1440000   177802     1323    61726      0.00081    9997.7694    9992.5322    9996.6401            0      1440000            0 
    8200     52.77727  1440000   177804     1248    61208      0.00082    9990.4505    10001.416    9998.3797            0      1440000            0 
    8300    79.466086  1440000   177792     1346    61800      0.00083     9992.229    10002.339    9995.4686            0      1440000            0 
    8400    106.13393  1440000   177794     1396    61773      0.00084    9996.4933    9995.4925    9995.7977            0      1440000            0 
    8500     132.9541  1440000   177795     1340    61799      0.00085    9997.6181    9994.5822    9995.1866            0      1440000            0 
    8600    159.62832  1440000   177804     1348    61722      0.00086    9994.2232    9996.0158    9998.1379            0      1440000            0 
    8700     187.3481  1440000   177796     1341    61510      0.00087    9996.6075      9993.23    9997.5008            0      1440000            0 
    8800    214.87852  1440000   177794     1334    62003      0.00088    9993.9715    10001.543    9993.9501            0      1440000            0 
    8900    238.90667  1440000   177809     1268    61426      0.00089    10001.656    9994.5228    9990.3198            0      1440000            0 
    9000     260.5075  1440000   177799     1329    61488       0.0009    9999.6604    9997.5688    9990.2608            0      1440000            0 
    9100    282.07335  1440000   177798     1321    61494      0.00091    9996.0457    10000.987    9991.8909            0      1440000            0 
    9200    303.86116  1440000   177800     1286    61426      0.00092    9991.9193    10005.905    9992.9435            0      1440000            0 
    9300    325.78259  1440000   177793     1348    61028      0.00093     9999.128    9996.2304    9991.9969            0      1440000            0 
    9400    344.54768  1440000   177804     1354    61698      0.00094    10000.798    9993.0403    9992.5622            0      1440000            0 
    9500    361.64738  1440000   177803     1362    61534      0.00095    9996.5365    9999.3506    9992.6277            0      1440000            0 
    9600    378.87962  1440000   177796     1363    61648      0.00096     9997.255    10002.497    9989.2039            0      1440000            0 
    9700    396.09241  1440000   177794     1223    61510      0.00097     9993.046    10008.181    9989.6869            0      1440000            0 
    9800    413.26279  1440000   177798     1300    61526      0.00098    9998.3356    10002.372    9987.9826            0      1440000            0 
    9900    429.88379  1440000   177782     1289    61291      0.00099    10004.533    9997.0581    9984.7565            0      1440000            0 
   10000    444.78259  1440000   177801     1311    61694        0.001    10002.534    10001.597    9983.5152            0      1440000            0 
Loop time of 444.783 on 2 procs for 2000 steps with 1440000 particles

MPI task timing breakdown:
Section |  min time  |  avg time  |  max time  |%varavg| %total
---------------------------------------------------------------
Move    | 37.973     | 38.104     | 38.236     |   2.1 |  8.57
Coll    | 233.17     | 235.93     | 238.68     |  17.9 | 53.04
Sort    | 29.813     | 29.822     | 29.832     |   0.2 |  6.70
Comm    | 5.2161     | 5.3865     | 5.5569     |   7.3 |  1.21
Modify  | 132.63     | 135.07     | 137.51     |  21.0 | 30.37
Output  | 0.46562    | 0.47034    | 0.47505    |   0.7 |  0.11
Other   |            | 0.001271   |            |       |  0.00

Particle moves    = 2880000000 (2.88B)
Cells touched     = 3695443823 (3.7B)
Particle comms    = 37049975 (37M)
Boundary collides = 74095243 (74.1M)
Boundary exits    = 0 (0K)
SurfColl checks   = 0 (0K)
SurfColl occurs   = 0 (0K)
Surf reactions    = 0 (0K)
Collide attempts  = 355599345 (356M)
Collide occurs    = 123140057 (123M)
Reactions         = 2646581 (2.65M)
Particles stuck   = 0

Particle-moves/CPUsec/proc: 3.23754e+06
Particle-moves/step: 1.44e+06
Cell-touches/particle/step: 1.28314
Particle comm iterations/step: 1
Particle fraction communicated: 0.0128646
Particle fraction colliding with boundary: 0.0257275
Particle fraction exiting boundary: 0
Surface-checks/particle/step: 0
Surface-collisions/particle/step: 0
Surf-reactions/particle/step: 0
Collision-attempts/particle/step: 0.123472
Collisions/particle/step: 0.042757
Reactions/particle/step: 0.000918952

Gas reaction tallies:

  reactions are frozen and don't really occur

  style tce #-of-reactions 3
  reaction O2 + O2 --> O + O + O2
    number of reactions of pseudo-particle: 2.64658e+06
    number of reactions of real particle: 1.8379e+17
    reaction rate: 9.18952e-18 (m^3/s)
    reaction rate: 5.53406e+12 (cm^3/mol/s)

Particles: 720000 ave 720728 max 719272 min
Histogram: 1 0 0 0 0 0 0 0 0 1
Cells:      72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
GhostCell: 72 ave 72 max 72 min
Histogram: 2 0 0 0 0 0 0 0 0 0
EmptyCell: 0 ave 0 max 0 min
Histogram: 2 0 0 0 0 0 0 0 0 0
write_restart       particle.restart

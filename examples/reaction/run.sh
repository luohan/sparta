#!/bin/bash
set -e
NP=2
TEMPS="5000 7500 10000 12500 15000 17500 20000"
EXECPATH=$(realpath  ../../../src/spa_mpi)

RUN_DIR=`pwd`
CASE=$(basename ${RUN_DIR})
INPUTNAME="in.reac"

i=0
for TEMP in ${TEMPS}; do
  cd ${RUN_DIR}
  #ii=$(printf "%03d" $i)

  CASE_DIR=${RUN_DIR}/"T=${TEMP}"
  mkdir -p ${CASE_DIR}

  cd ${CASE_DIR}
  execname="spa_mpi_${CASE}_${TEMP}"
  ln -sf $EXECPATH ./${execname}
  ln -sf ../air* ./
  ln -sf ../*.json ./

  cp "${RUN_DIR}/${INPUTNAME}" ./
  sed -i "s|Teq\s*equal.*|Teq equal ${TEMP}.0|" ${INPUTNAME}
  j=`echo "$i+${NP}-1" | bc -l`
  nohup mpiexec --cpu-set ${i}-${j} -n ${NP}  --use-hwthread-cpus ./${execname} < ${INPUTNAME} > stdout.log 2>&1 &
  echo ${CASE_DIR}
  i=$((i+${NP}))
done






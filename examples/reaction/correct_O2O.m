clear
clc
Boltzmann = 1.38064852e-23;
N_A = 6.022140857e23;
phys = loadPhysConst;
%% CFD rate
Apark = [1e22/N_A*1e-6 -1.5 59500*Boltzmann]; %unit: m^3/s, 1, joule
A = [2.500d18/N_A*1e-6  -0.565  60491.819*Boltzmann]; %unit: m^3/s, 1, joule  Marat's rate
A = [4.15135e-12  -0.565  8.35175e-19]; %unit: m^3/s, 1, joule  Marat's rate, truncated

% T = 500:100:20000;
% semilogy(T, Apark(1)*T.^Apark(2).*exp(-Apark(3)./T/Boltzmann), T, A(1)*T.^A(2).*exp(-A(3)./T/Boltzmann))
%% Load SPARTA results
fid = fopen('O2-O-Uncorrect.tec','r');
for i = 1:2
    fgetl(fid);
end
data = fscanf(fid,'%f');
fclose(fid);
data = reshape(data,2,[])';
T = data(:,1);
rate = data(:,2);
%semilogy(T, rate, T, A(1)*T.^A(2).*exp(-A(3)./T/Boltzmann))
%% Fit DSMC calculated rate
[a,b,gof] = FitArRate( T, rate, A(3)/Boltzmann, A );
semilogy(T, rate, T, a*T.^b.*exp(-A(3)./T/Boltzmann));
%% Get corrected rate
% Theory is based on 10.1063/1.1751332
Anew = [A(1)^2/a, 2*A(2) - b, A(3)];
fprintf('New Arrhenius parameters:\n');
fprintf('A = %.6e (m^3/s)\n',Anew(1));
fprintf('eta = %.6e (m^3/s)\n',Anew(2));
fprintf('E = %.6e (Joule)\n',Anew(3));
fprintf('D A 1.0 %.6e %.6e %.6f %.6e\n',Anew(3),Anew(1),Anew(2),-Anew(3));


%% Inline function
function [a,b,gof] = FitArRate( Tfit, Rate, Diss, Ar0 )
% fit reaction rates
fitform = sprintf('a+b*log(x)-%f/x',Diss);
ft = fittype( fitform, 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [log(Ar0(1)) Ar0(2)];
[x,y] = prepareCurveData(Tfit,Rate);
[fitresult, gof] = fit(x, log(y), ft, opts );
coeff = coeffvalues(fitresult);
coeff(1) = exp(coeff(1));
a = coeff(1); b = coeff(2);
end

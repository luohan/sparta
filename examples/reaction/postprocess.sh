#!/bin/bash
set -e
NP=2
TEMPS="5000 7500 10000 12500 15000 17500 20000"
NTEMP=$(echo $TEMPS|awk '{print NF}')
RUN_DIR=`pwd`
CASE=$(basename ${RUN_DIR})
INPUTNAME="in.reac"

cat > ${CASE}.tec <<EOF
VARIABLES="T (K)","Rate (m^3/s)"
ZONE T="${CASE} SPARTA Rate" I=${NTEMP}
EOF

i=0
for TEMP in ${TEMPS}; do
  cd ${RUN_DIR}
  #ii=$(printf "%03d" $i)

  CASE_DIR=${RUN_DIR}/"T=${TEMP}"
  mkdir -p ${CASE_DIR}
  cd ${CASE_DIR}
  mv log.sparta log.${CASE}
  RATE=$(awk -F '[: ]+' '
     /\(m\^3/ {
        num+=1
	sum+=$4
     }
     END {
        printf "%.6e", sum/num
}' log.${CASE})
  cat >> ../${CASE}.tec <<EOF
${TEMP} ${RATE}
EOF
done






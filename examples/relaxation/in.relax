# test of vibrational energy modes
# thermal gas in a 3d box with collisions
# particles reflect off global box boundaries
# Tips:
# - To create particles with vibrational energy, `collide_modify vibrate discrete` needs to be ahead of create_particles
#
variable            mfp0  equal 0.00178   #mfp for 1000K,1e21
variable            mct   equal 2.169e-6
variable            n0    equal 1E21
variable            boxsize equal 0.01
variable            GNX0  equal ceil(${boxsize}/${mfp0})*2 # every grid is half mfp size
variable            GNY0  equal ceil(${boxsize}/${mfp0})*2
variable            FNUM  equal ${n0}*${boxsize}*${boxsize}/(${GNX0}*${GNY0}*1e4) #every cell has 1e4 molecules
variable            zoomfactor equal 1/${boxsize}
variable            NE equal 20
variable            ND equal 100
variable            NR equal 500
variable            NMOL equal ${n0}*${boxsize}*${boxsize}/${FNUM}
print               "NMOL=${n0}*${boxsize}*${boxsize}/${FNUM}"
print               "FNUM=${FNUM}"
print               "GNX0=${GNX0} GNY0=${GNY0}"
print               "NMOL=${NMOL}"
timestep            5e-7
seed	    	    1234
global              nrho ${n0} fnum ${FNUM} comm/sort yes

# grid info
boundary            r r p
create_box          0 ${boxsize} 0 ${boxsize} -0.5 0.5
create_grid         ${GNX0} ${GNY0} 1

# surf info

# write grid
balance_grid        rcb cell
fix                 gridcheck grid/check 1 warn
write_grid          parent Grid0_partent.grid
write_grid          geom   Grid0_geom.grid

# particle info
species		    air.species O2
mixture             background O2 frac 1.0 temp 5000 trot 5000 tvib 300 group all
collide             vss background air.vss relax variable isothermal yes relaxtemp temp
collide_modify      rotate smooth vibrate discrete
create_particles    background n 0

# compute
compute             1    grid         all all n nrho massrho u v
compute             2    thermal/grid all all temp press
compute             3    tvib/grid    all species
compute             4    trot/grid    all all
variable            ftime equal dt*step

# fix
fix                 1   ave/grid     all 1 ${NE} ${NE} c_2[1] c_4[1] c_3[*] ave one
fix                 2   ave/grid     all 1 ${NE} ${NE} c_1[*]
#compute             5   reduce ave   c_2[1] c_1[7] c_5[1] c_3[1]
compute             5   reduce ave   f_1[*]

# modify dependent on fix
# 1. use cell temperature for relaxation
collide_modify      temp f_1[*]


# dump
dump                1 grid  all ${ND} flowfield.out.* id xc yc f_1[*] f_2[*]
#dump               3 image all 50 dump.*.ppm type type pdiam 0.0001 view 0.0 0.0 size 1024 1024 zoom ${zoomfactor} gline yes 0.01

# stat
stats_style	    step cpu np nattempt ncoll v_ftime c_5[*]
stats		    ${NE}

# restart
run 		    30000
write_restart       particle.restart

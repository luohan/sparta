#!/usr/bin/env python3

#%%
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as co

nskip = 125
nsteps = 30000
nevery = 20
nrows = int(nsteps / nevery + 1)
dt = 5e-7
with open('log.relaxation', 'r') as f:
    data = np.loadtxt(f, skiprows=nskip, max_rows=nrows)


# %%
T0 = 300
Tt = 5000
thetaV = 2256
dref = 4.1515e-10
Tref = 273.15
omega = 0.7318
mr = 32 * co.m_u / 2
nd = 1e21
C1 = 56.5
C2 = 153.5

time = data[:, 0] * dt
Ev = co.Boltzmann * thetaV / (np.exp(thetaV / data[:, -1]) - 1.0)
Ev0 = co.Boltzmann * thetaV / (np.exp(thetaV / T0) - 1.0)
EvEq = co.Boltzmann * thetaV / (np.exp(thetaV / Tt) - 1.0)
collFreq = 2 * np.sqrt(np.pi) * dref**2 * (Tt / Tref)**(1 - omega) * np.sqrt(2.0 * co.Boltzmann * Tref / mr)
tcoll = 1 / (nd * collFreq)
Zv = C1 / Tt**omega * np.exp(C2 * Tt**(-1 / 3))

#%%
plt.rcParams.update({"figure.figsize": (20, 10)})
plt.rcParams.update({"font.size": 20})
fig, axs = plt.subplots(1, 2)
axs[0].plot(time / tcoll / Zv, 1 - Ev / EvEq)
axs[0].set_title('SPARTA simulation')
axs[0].set_xlabel(r'$\tilde{t}/Z$')
axs[0].set_ylabel(r'$1-E_v/E_v^*(\infty)$')
axs[0].set_xlim(0, 4)
axs[0].set_ylim(0, 1)

axs[1].plot(time / tcoll / Zv, np.exp(-time / tcoll / Zv))
axs[1].set_title('Theory')
axs[1].set_xlabel(r'$\tilde{t}/Z$')
axs[1].set_xlim(0, 4)
axs[1].set_ylim(0, 1)

fig.savefig('phiplot.png')


# %%
